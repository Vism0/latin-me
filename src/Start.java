import java.io.IOException;
import java.net.URISyntaxException;

import Utils.Utils;
import WindowsUpdate.CheckDate;

public class Start {

	public static void main(String[] args) throws IOException, URISyntaxException {
		
		/* Crea cartella dove salver i file */
		Utils.CreateFolder();
				
		/* Avvia app */
		Main.main(args);
		
		/* Cerca aggiornamenti */
		CheckDate.main(args);
	}

}
