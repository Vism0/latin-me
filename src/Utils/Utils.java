package Utils;

import java.io.File;

import Misc.FolderError;

public class Utils {
	private static File folder = new File(System.getProperty("user.home"), "/LatinME");
	public static void CreateFolder() {
		if (folder.exists() && folder.isDirectory()) {
			return;
		} else if (!folder.exists()) {
			createFolder();
			return;
		}
		
	}
	
	private static void createFolder() {
		if (folder.mkdir()) {
            System.out.println("Cartella creata!");
            return;
        } else {
        	String[] args = null;
			FolderError.main(args);
            System.out.println("Impossibile creare la cartella!");
            return;
        }	
	}
}
