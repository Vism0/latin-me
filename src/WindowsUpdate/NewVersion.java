package WindowsUpdate;

import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class NewVersion {

	private static final String downloadUrl = "https://bitbucket.org/Astrubale/latin-me/downloads/";

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					NewVersion window = new NewVersion();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public NewVersion() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Aggiornamento");
		frame.setBounds(100, 100, 462, 205);
		
		JLabel lblUnaNuovaVersione = new JLabel("Una nuova versione \u00E8 disponibile.");
		lblUnaNuovaVersione.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSiDesideraScaricarla = new JLabel("Si desidera scaricarla?");
		lblSiDesideraScaricarla.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnSi = new JButton("SI");
		btnSi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnSi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Desktop show = Desktop.getDesktop();
				try {
					show.browse(new URI(downloadUrl));
					frame.setVisible(false);
					return;
				} catch (IOException | URISyntaxException e) {
					String[] args = null;
					UpdateError.main(args);
					e.printStackTrace();
					return;
				}}
		});
		
		JButton btnNo = new JButton("NO");
		btnNo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnNo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				return;
			}
		});
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblUnaNuovaVersione)
						.addGroup(Alignment.TRAILING, groupLayout.createSequentialGroup()
							.addComponent(btnSi)
							.addPreferredGap(ComponentPlacement.RELATED, 169, Short.MAX_VALUE)
							.addComponent(btnNo))
						.addComponent(lblSiDesideraScaricarla))
					.addContainerGap())
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblUnaNuovaVersione)
					.addPreferredGap(ComponentPlacement.RELATED, 42, Short.MAX_VALUE)
					.addComponent(lblSiDesideraScaricarla)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNo)
						.addComponent(btnSi))
					.addContainerGap())
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
