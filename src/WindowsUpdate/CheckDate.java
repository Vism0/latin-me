package WindowsUpdate;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;


public class CheckDate {

	private static File dir = new File(System.getProperty("user.home"), "/LatinME");
	
	private static DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    private static Date today = Calendar.getInstance().getTime();
	
	private static File date = new File (dir + "/Update_date.txt");
	
	private static BufferedReader in;

	private static int set;
	
	public static void main(String[] args) throws IOException, URISyntaxException {
		System.out.println("Vedo se il file della data esiste");
		if (checkFile() == true) {
			System.out.println("Comparo le date");
			compare();
			if (set == 1) {
				Update.main(args);
				System.out.println("Scrivo il file della data");
				createFile();
				return;
			} else if (set == 0) {
				System.out.println("Scrivo il file della data");
				createFile();
				System.out.println("Non avvio l'aggiornamento");
				return;
			}
		} else if (checkFile() == false) {
			System.out.println("Avvio aggiornamento");
			Update.main(args);
			System.out.println("Scrivo il file della data");
			createFile();
			return;
		}		
		return;
	}
	
	private static void compare() throws IOException {
		in = new BufferedReader(new FileReader(date.toString()));
        
        String line;
        
        while((line = in.readLine()) != null) {
        	if (!line.toString().equals( df.format(today).toString())){
        		System.out.println("Il file non � di oggi");
        		set = 1;
        		return;
        	} else if (line.toString().equals( df.format(today).toString())){
        		System.out.println("Il file � di oggi");
        		set = 0;
        		return;
        	}
        }
        in.close();
        return;
	}

	private static void createFile () throws IOException {
		if (checkFile() == true) {
			System.out.println("Elimino il file che devo sostituire");
			date.delete();
		}
		
		try {
			System.out.println("Salvo il file della data");
	        String reportDate = df.format(today);
	        String dateToPrintToFile = reportDate;
	        
	        FileWriter fw = new FileWriter(date.getAbsoluteFile());
	        BufferedWriter bw = new BufferedWriter(fw);
	        bw.write(dateToPrintToFile);
	        bw.close();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
		return;
	}

	private static boolean checkFile() {
		if (date.exists()) {
			System.out.println("Il file esiste gi�");
			return true;
		} else 
			System.out.println("Il file non esiste");
		return false;
	}
}
