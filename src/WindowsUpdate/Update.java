package WindowsUpdate;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class Update {

	public static final String version = "0.3"; 
	static final String newLine = System.getProperty("line.separator");
	
	private static final String url = "https://bitbucket.org/Astrubale/latin-version/downloads/Version.txt";
	private static final File versionFile = new File(System.getProperty("user.home"), "/LatinME/Version.txt");
    private static final String downloadErr = newLine + "ERRORE: impossibile scaricare il file" + newLine;
    private static final String serverErr = newLine + "ERRORE: impossibile connettersi al server" + newLine;
    private static final String checkServerErr = newLine + "ERRORE: impossibile controllare se il server sia disponibile" + newLine;
    private static final String compareErr = newLine + "ERRORE: impossibile confrontare le versioni" + newLine;
    private static final String deleteErr = newLine + "ERRORE: impossibile cancellare il file scaricato" + newLine;
    private static final String bitBucketUrl = "https://bitbucket.org/";
	private static BufferedReader in;
    
	public static void main(String[] args) throws IOException, URISyntaxException {		
		String[] del = {versionFile.toString()};
        for (String remove : del) {
        	if (new File(remove).exists()) {
        		deleteFile();
        		return;
            }
        }

		if (checkBitBucket()==true) {
			/* Scarica file versione */
	        
	        try {
	            downloadUsingNIO(url, versionFile.toString());
	            
	        } catch (IOException e) {
	        	UpdateError.main(args);
	            e.printStackTrace();
	            return;
	        }
	        
	        /* Controlla se il file � stato scaricato */ 

			String[] paths = {versionFile.toString()};
	        for (String path : paths) {
	            
	        	if (new File(path).exists()) {
	        		compareVersions();
	        		return;
	        		
	            } else 
	            	UpdateError.main(args);
	            	System.out.println(downloadErr);
	            	return;
	        }
		} else if (checkBitBucket()==false) {
			UpdateError.main(args);
			System.out.println(serverErr);
			return;
			
		} else 
			UpdateError.main(args);
			System.out.println(checkServerErr);
			return;
	}
    				
		
    private static void downloadUsingNIO(String urlStr, String file) throws IOException {
        URL url = new URL(urlStr);
        ReadableByteChannel rbc = Channels.newChannel(url.openStream());
        FileOutputStream fos = new FileOutputStream(file);
        fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
        fos.close();
        rbc.close();
    }
    
    /* Confronta le versioni  */ 

    private static void compareVersions() throws IOException, URISyntaxException{
        in = new BufferedReader(new FileReader(versionFile.toString()));
        
        String line;
        
        while((line = in.readLine()) != null) {
        	if (Double.parseDouble(line) < Double.parseDouble(version)){
        		String[] args = null;
				UpdateError.main(args);
        		System.out.println(compareErr);
        		return;
        		
        	} else if (Double.parseDouble(line) == Double.parseDouble(version)){
        		String[] args = null;
				Updated.main(args);
				return;
        		
        	} else if (Double.parseDouble(line) > Double.parseDouble(version)){        		
        		String[] args = null;
				NewVersion.main(args);
				return;
        	}
        }
        in.close();
    }
    
    private static void deleteFile(){
		String[] args = null;
		if(versionFile.delete()) {
			return;
		} else
			UpdateError.main(args);
		System.out.println(deleteErr);
		return;
    }
	
	/* Controlla se il server � disponibile */
	
	private static boolean checkBitBucket() {
	    try {
	        final URL url = new URL(bitBucketUrl);
	        final URLConnection conn = url.openConnection();
	        conn.connect();
	        return true;
	        
	    } catch (MalformedURLException e) {
	        throw new RuntimeException(e);
	        
	    } catch (IOException e) {
	        return false;
	    }
	}	
}
