import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

import Data.accusativo;
import Data.accusativo_greca;
import Data.aggettivi_prima_classe;
import Data.aggettivi_seconda_classe;
import Data.concessiva;
import Data.dativo;
import Data.declinazione_quinta;
import Data.declinazione_seconda;
import Data.declinazione_terzaA;
import Data.declinazione_terzaC;
import Data.declionazione_quarta;
import Data.fero_fers;
import Data.finale;
import Data.genitivo;
import Data.gerundio;
import Data.imperativo;
import Data.participio;
import Data.perifrastica_attiva;
import Data.perifrastica_passiva;
import Data.pluralia_tantum;
import Data.pronomi_dimostrativi;
import Data.pronomi_indefinitivi;
import Data.pronomi_riflessivi;
import Data.semideponenti;
import Data.supino;
import Data.temporale;
import Data.vocativo;
import Data.volo_nolo_malo;

public class Main {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Latin ME");
		frame.setBounds(100, 100, 1199, 507);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JLabel lblDeclinazioni = new JLabel("Declinazioni");
		lblDeclinazioni.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblUsoDeiCasi = new JLabel("Uso dei casi");
		lblUsoDeiCasi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNewLabel = new JLabel("Pronomi");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVerbi = new JLabel("Verbi");
		lblVerbi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAnalisiDelPeriodo = new JLabel("Analisi del periodo");
		lblAnalisiDelPeriodo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnDeclinazione = new JButton("1 Declinazione");
		btnDeclinazione.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnNominativo = new JButton("Nominativo");
		btnNominativo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnPersonali = new JButton("Personali");
		btnPersonali.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnIndicativo = new JButton("Indicativo");
		btnIndicativo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnFinale = new JButton("Finale");
		btnFinale.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnFinale.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				finale.main(args);
			}
		});
		
		JButton btnDeclinazione_1 = new JButton("2 Declinazione");
		btnDeclinazione_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnDeclinazione_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				declinazione_seconda.main(args);
			}
		});
		
		JButton btnGenitivo = new JButton("Genitivo");
		btnGenitivo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnGenitivo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				genitivo.main(args);
			}
		});
		
		JButton btnDimostrativi = new JButton("Dimostrativi");
		btnDimostrativi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnDimostrativi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				pronomi_dimostrativi.main(args);
			}
		});
		
		JButton btnCongiuntivo = new JButton("Congiuntivo");
		btnCongiuntivo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnIpotetica = new JButton("Ipotetica");
		btnIpotetica.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnaDeclinazione = new JButton("3a Declinazione");
		btnaDeclinazione.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnaDeclinazione.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				declinazione_terzaA.main(args);
			}
		});
		
		JButton btnDativo = new JButton("Dativo");
		btnDativo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnDativo.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				dativo.main(args);
			}
		});		
		
		JButton btnRelativi = new JButton("Relativi");
		btnRelativi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnInfinito = new JButton("Infinito");
		btnInfinito.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnCausale = new JButton("Causale");
		btnCausale.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnbDeclinazione = new JButton("3b Declinazione");
		btnbDeclinazione.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnAccusativo = new JButton("Accusativo");
		btnAccusativo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnAccusativo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				accusativo.main(args);
			}
		});
		
		JButton btnRiflessivi = new JButton("Riflessivi");
		btnRiflessivi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnRiflessivi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				pronomi_riflessivi.main(args);
			}
		});
		
		JButton btnParticipio = new JButton("Participio");
		btnParticipio.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnParticipio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				participio.main(args);
			}
		});
		
		JButton btnTemporale = new JButton("Temporale");
		btnTemporale.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnTemporale.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				temporale.main(args);
			}
		});
		
		JButton btncDeclinazione = new JButton("3c Declinazione");
		btncDeclinazione.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btncDeclinazione.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				declinazione_terzaC.main(args);
			}
		});
		
		JButton btnAblativo = new JButton("Ablativo");
		btnAblativo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnIndefiniti = new JButton("Indefiniti");
		btnIndefiniti.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnIndefiniti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				pronomi_indefinitivi.main(args);
			}
		});
		
		JButton btnGerundio = new JButton("Gerundio");
		btnGerundio.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnGerundio.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				gerundio.main(args);
			}
		});
		
		JButton btnCondizionale = new JButton("Condizionale");
		btnCondizionale.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnDeclinazione_2 = new JButton("4 Declinazione");
		btnDeclinazione_2.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnDeclinazione_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				declionazione_quarta.main(args);
			}
		});
		
		JButton btnLocativo = new JButton("Vocativo");
		btnLocativo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnLocativo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				vocativo.main(args);
			}
		});
		
		JButton btnMisti = new JButton("Misti");
		btnMisti.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnImperativo = new JButton("Imperativo");
		btnImperativo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnImperativo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				imperativo.main(args);
			}
		});
		
		JButton btnConcessiva = new JButton("Concessiva");
		btnConcessiva.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnConcessiva.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				concessiva.main(args);
			}
		});
		
		JButton btnDeclinazione_3 = new JButton("5 Declinazione");
		btnDeclinazione_3.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnDeclinazione_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				declinazione_quinta.main(args);
			}
		});
		
		JButton btnDoppi = new JButton("Doppi");
		btnDoppi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnSupino = new JButton("Supino");
		btnSupino.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnSupino.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				supino.main(args);
			}
		});
		
		JButton btnAggettiviClasse = new JButton("Aggettivi 1 classe");
		btnAggettiviClasse.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnAggettiviClasse.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				aggettivi_prima_classe.main(args);
			}
		});
		
		JButton btnAggettiviClasse_1 = new JButton("Aggettivi 2 classe");
		btnAggettiviClasse_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnAggettiviClasse_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				aggettivi_seconda_classe.main(args);
			}
		});
		
		JLabel lblCoseMagnifiche = new JLabel("Cose magnifiche");
		lblCoseMagnifiche.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnDeponenti = new JButton("Deponenti");
		btnDeponenti.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnSemideponenti = new JButton("Semi-deponenti");
		btnSemideponenti.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnSemideponenti.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				semideponenti.main(args);
			}
		});
		
		JButton btnCompostiDiSum = new JButton("Composti di sum");
		btnCompostiDiSum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnVoloNoloMalo = new JButton("Volo, nolo, malo");
		btnVoloNoloMalo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnVoloNoloMalo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				volo_nolo_malo.main(args);
			}
		});
		
		JButton btnNewButton_1 = new JButton("Eo, is, ivi, itum, ire");
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnFeroFersTuli = new JButton("Fero, fers, tuli, latum, ferre");
		btnFeroFersTuli.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnFeroFersTuli.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				fero_fers.main(args);
			}
			
		});
		
		JButton btnVideorEris = new JButton("Videor, eris");
		btnVideorEris.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNews = new JLabel("News");
		lblNews.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnAccusativoAllaGreca = new JButton("Accusativo alla greca");
		btnAccusativoAllaGreca.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnAccusativoAllaGreca.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				accusativo_greca.main(args);
			}
		});
		
		JButton btnPluraliaTantum = new JButton("Pluralia tantum");
		btnPluraliaTantum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnPluraliaTantum.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				pluralia_tantum.main(args);
			}
		});
		
		JButton btnAblativoAssoluto = new JButton("Ablativo assoluto");
		btnAblativoAssoluto.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnCumNarrativo = new JButton("CUM narrativo");
		btnCumNarrativo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnPerifrasticaAttiva = new JButton("Perifrastica attiva");
		btnPerifrasticaAttiva.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnPerifrasticaAttiva.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				perifrastica_attiva.main(args);
			}
		});
		
		JButton btnPerifrasticaPassiva = new JButton("Perifrastica passiva");
		btnPerifrasticaPassiva.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnPerifrasticaPassiva.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				perifrastica_passiva.main(args);
			}
			
		});
		
		JButton btnRelativa = new JButton("Relativa");
		btnRelativa.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblRealizzataDaTommaso = new JLabel("Realizzata da: Tommaso Movis");
		lblRealizzataDaTommaso.setFont(new Font("Tahoma", Font.PLAIN, 17));
		lblRealizzataDaTommaso.setForeground(Color.GRAY);
		
		JButton btnInfo = new JButton("Info");
		btnInfo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnInfo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				String[] args = null;
				Info.main(args);
			}
		});
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addComponent(lblRealizzataDaTommaso))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(7)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(lblDeclinazioni, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnaDeclinazione, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnbDeclinazione, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnAggettiviClasse_1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnDeclinazione_3, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnAggettiviClasse, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnDeclinazione_2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btncDeclinazione, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnDeclinazione_1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnDeclinazione, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(btnDativo, GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
								.addComponent(btnAccusativo, GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
								.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
									.addComponent(btnGenitivo, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(btnNominativo, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addComponent(lblUsoDeiCasi, GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
								.addComponent(btnAblativo, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
								.addComponent(btnLocativo, GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(btnPersonali, GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE)
								.addComponent(btnDimostrativi, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnRelativi, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnRiflessivi, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnIndefiniti, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnMisti, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnDoppi, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(lblNewLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING)
								.addComponent(lblVerbi, GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE)
								.addComponent(btnGerundio, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE)
								.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
									.addComponent(btnParticipio, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(btnInfinito, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(btnIndicativo, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(btnCongiuntivo, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addComponent(btnImperativo, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE)
								.addComponent(btnSupino, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 91, Short.MAX_VALUE))
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(btnFinale, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
								.addComponent(btnIpotetica, GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
								.addComponent(lblAnalisiDelPeriodo, GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
								.addComponent(btnTemporale, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
								.addComponent(btnConcessiva, GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
								.addComponent(btnCondizionale, GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
								.addComponent(btnCausale, Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
								.addComponent(btnRelativa, GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE))))
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
									.addComponent(btnFeroFersTuli, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(btnNewButton_1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(btnVideorEris, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(btnVoloNoloMalo, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(btnSemideponenti, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(lblCoseMagnifiche, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
								.addComponent(btnDeponenti, GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE)
								.addComponent(btnCompostiDiSum, GroupLayout.DEFAULT_SIZE, 167, Short.MAX_VALUE))
							.addPreferredGap(ComponentPlacement.RELATED)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING, false)
								.addComponent(btnAccusativoAllaGreca, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnPluraliaTantum, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnAblativoAssoluto, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnCumNarrativo, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnPerifrasticaAttiva, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnPerifrasticaPassiva, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(lblNews, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
							.addGap(150))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(23)
							.addComponent(btnInfo)
							.addContainerGap())))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGap(8)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDeclinazioni)
						.addComponent(lblUsoDeiCasi)
						.addComponent(lblNewLabel)
						.addComponent(lblVerbi)
						.addComponent(lblAnalisiDelPeriodo)
						.addComponent(lblCoseMagnifiche)
						.addComponent(lblNews))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnDeclinazione)
						.addComponent(btnNominativo)
						.addComponent(btnPersonali)
						.addComponent(btnIndicativo)
						.addComponent(btnFinale)
						.addComponent(btnAccusativoAllaGreca)
						.addComponent(btnDeponenti))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnDeclinazione_1)
						.addComponent(btnGenitivo)
						.addComponent(btnDimostrativi)
						.addComponent(btnCongiuntivo)
						.addComponent(btnIpotetica)
						.addComponent(btnSemideponenti)
						.addComponent(btnPluraliaTantum))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnaDeclinazione)
						.addComponent(btnDativo)
						.addComponent(btnRelativi)
						.addComponent(btnInfinito)
						.addComponent(btnAblativoAssoluto)
						.addComponent(btnCompostiDiSum)
						.addComponent(btnCausale))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnbDeclinazione)
						.addComponent(btnAccusativo)
						.addComponent(btnRiflessivi)
						.addComponent(btnParticipio)
						.addComponent(btnTemporale)
						.addComponent(btnVoloNoloMalo)
						.addComponent(btnCumNarrativo))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btncDeclinazione)
						.addComponent(btnIndefiniti)
						.addComponent(btnGerundio)
						.addComponent(btnCondizionale)
						.addComponent(btnNewButton_1)
						.addComponent(btnPerifrasticaAttiva)
						.addComponent(btnLocativo))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnDeclinazione_2)
						.addComponent(btnMisti)
						.addComponent(btnImperativo)
						.addComponent(btnConcessiva)
						.addComponent(btnFeroFersTuli)
						.addComponent(btnPerifrasticaPassiva)
						.addComponent(btnAblativo))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(btnDeclinazione_3)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(btnAggettiviClasse)
							.addGap(4)
							.addComponent(btnAggettiviClasse_1))
						.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
							.addComponent(btnDoppi)
							.addComponent(btnSupino)
							.addComponent(btnVideorEris)
							.addComponent(btnRelativa)))
					.addPreferredGap(ComponentPlacement.RELATED, 55, Short.MAX_VALUE)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblRealizzataDaTommaso)
						.addComponent(btnInfo))
					.addContainerGap())
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
