import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class Info {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Info window = new Info();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Info() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Info");
		frame.setBounds(100, 100, 679, 428);
		
		JLabel lblAppCreataAllo = new JLabel("App creata solo per scopo educativo.");
		lblAppCreataAllo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblIContenutiDi = new JLabel("I contenuti di quest'app potrebbero essere soggeti a copyright.");
		lblIContenutiDi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblLibrerieUtilizzate = new JLabel("Librerie utilizzate:");
		lblLibrerieUtilizzate.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblJgoodiescommonsourcesjar = new JLabel("- jgoodies-common-1.8.0-sources.jar");
		lblJgoodiescommonsourcesjar.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblJgoodiescommonjar = new JLabel("- jgoodies-common-1.8.0.jar");
		lblJgoodiescommonjar.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblJgoodiesformssourcesjar = new JLabel("- jgoodies-forms-1.8.0-sources.jar");
		lblJgoodiesformssourcesjar.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblJgoodiesformsjar = new JLabel("- jgoodies-forms-1.8.0.jar");
		lblJgoodiesformsjar.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblMiglayoutsrczip = new JLabel("- miglayout-src.zip");
		lblMiglayoutsrczip.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblMiglayoutswingjar = new JLabel("- miglayout15-swing.jar");
		lblMiglayoutswingjar.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JButton btnChiudi = new JButton("Chiudi");
		btnChiudi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		btnChiudi.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
				return;
			}
		});
		
		JLabel lblContatti = new JLabel("Contatti:");
		lblContatti.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblAstrubaledevelopergmailcom = new JLabel("astrubale.developer@gmail.com");
		lblAstrubaledevelopergmailcom.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.TRAILING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblAppCreataAllo)
						.addComponent(lblIContenutiDi)
						.addComponent(lblLibrerieUtilizzate)
						.addComponent(lblJgoodiescommonsourcesjar)
						.addComponent(lblJgoodiescommonjar)
						.addComponent(lblJgoodiesformssourcesjar)
						.addComponent(lblJgoodiesformsjar)
						.addComponent(lblMiglayoutsrczip)
						.addComponent(lblMiglayoutswingjar))
					.addContainerGap(120, Short.MAX_VALUE))
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap(363, Short.MAX_VALUE)
					.addComponent(btnChiudi)
					.addContainerGap())
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblContatti)
					.addContainerGap(378, Short.MAX_VALUE))
				.addGroup(Alignment.LEADING, groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblAstrubaledevelopergmailcom)
					.addContainerGap(378, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblAppCreataAllo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblIContenutiDi)
					.addGap(18)
					.addComponent(lblLibrerieUtilizzate)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblJgoodiescommonsourcesjar)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblJgoodiescommonjar)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblJgoodiesformssourcesjar)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblJgoodiesformsjar)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblMiglayoutsrczip)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblMiglayoutswingjar)
					.addGap(18)
					.addComponent(lblContatti)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAstrubaledevelopergmailcom)
					.addPreferredGap(ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
					.addComponent(btnChiudi)
					.addContainerGap())
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
