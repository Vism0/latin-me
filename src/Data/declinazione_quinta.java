package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class declinazione_quinta {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					declinazione_quinta window = new declinazione_quinta();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public declinazione_quinta() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Quinta declinazione");
		frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frame.setBounds(100, 100, 507, 307);
		
		JLabel lblNewLabel = new JLabel("SINGOLARE PLURALE ");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblNewLabel_1 = new JLabel("Nom: Dies Dies ");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenDieiDierum = new JLabel("Gen: Diei Dierum ");
		lblGenDieiDierum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDatDieiDiebus = new JLabel("Dat: Diei Diebus ");
		lblDatDieiDiebus.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccDiemDies = new JLabel("Acc: Diem Dies ");
		lblAccDiemDies.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVocDiesDies = new JLabel("Voc: Dies Dies ");
		lblVocDiesDies.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblDieDiebus = new JLabel("Abl: Die Diebus ");
		lblAblDieDiebus.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSingolarePlurale = new JLabel("SINGOLARE PLURALE");
		lblSingolarePlurale.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblNomResRes = new JLabel("Nom: Res Res ");
		lblNomResRes.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenReiRerum = new JLabel("Gen: Rei Rerum ");
		lblGenReiRerum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDatReiRebus = new JLabel("Dat: Rei Rebus ");
		lblDatReiRebus.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccRemRes = new JLabel("Acc: Rem Res ");
		lblAccRemRes.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVocResRes = new JLabel("Voc: Res Res ");
		lblVocResRes.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblReRebus = new JLabel("Abl: Re Rebus");
		lblAblReRebus.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblNewLabel)
						.addComponent(lblNewLabel_1)
						.addComponent(lblGenDieiDierum)
						.addComponent(lblDatDieiDiebus)
						.addComponent(lblAccDiemDies)
						.addComponent(lblVocDiesDies)
						.addComponent(lblAblDieDiebus))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblAblReRebus)
						.addComponent(lblVocResRes)
						.addComponent(lblAccRemRes)
						.addComponent(lblDatReiRebus)
						.addComponent(lblGenReiRerum)
						.addComponent(lblNomResRes)
						.addComponent(lblSingolarePlurale))
					.addContainerGap(242, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel)
						.addComponent(lblSingolarePlurale))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNewLabel_1)
						.addComponent(lblNomResRes))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblGenDieiDierum)
						.addComponent(lblGenReiRerum))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDatDieiDiebus)
						.addComponent(lblDatReiRebus))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAccDiemDies)
						.addComponent(lblAccRemRes))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblVocDiesDies)
						.addComponent(lblVocResRes))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAblDieDiebus)
						.addComponent(lblAblReRebus))
					.addContainerGap(116, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
