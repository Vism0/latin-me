package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class temporale {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					temporale window = new temporale();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public temporale() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Temporale");
		frame.setBounds(100, 100, 1729, 461);
		
		JLabel lblEsprimonoLaCircostanza = new JLabel("Esprimono la circostanza temporale in cui si colloca l'azione della reggente.");
		lblEsprimonoLaCircostanza.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblInItalianoSono = new JLabel("In italiano sono introdotte da congiunzioni temporali e possono avere il verbo sia all'indicativo, sia al congiuntivo:");
		lblInItalianoSono.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblCumIndicativo = new JLabel("cum + indicativo = quando; col valore iterativo di \"tutte le volte che\" ha l'indicativo dei tempi composti, ma questi si traducono con i corrispondenti tempi semplici;");
		lblCumIndicativo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblCumAccompagnataDa = new JLabel("Cum accompagnata da subito o repente assume il significato di \"quand'ecco che, ed ecco che\" (cum inversum)");
		lblCumAccompagnataDa.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblCumPrimumUbi = new JLabel("cum primum, ubi, ubi primum, ut, ut primum, simul ac (atque), simul ut, statim ut + indicativo = appena, come, non appena;");
		lblCumPrimumUbi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblPostquamPosteaquamPostea = new JLabel("postquam, posteaquam, post(ea)... quam + indicativo = dopo che;");
		lblPostquamPosteaquamPostea.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAntequamPriusquamAnte = new JLabel("antequam, priusquam, ante... quam, prius... quam + indicativo o congiuntivo = prima che;");
		lblAntequamPriusquamAnte.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSiUsaIl = new JLabel("si usa il congiuntivo quando c'\u00E8 idea di eventualit\u00E0 o intenzione;");
		lblSiUsaIl.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDumIndicativo = new JLabel("dum + indicativo presente = mentre; indica sempre contemporaneit\u00E0 (in italiano il presente si traduce con il presente o con l'imperfetto, a seconda che nella proposizione reggente si trovi un tempo principale o storico)");
		lblDumIndicativo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDumDonecQuoad = new JLabel("dum, donec, quoad, quamdiu + indicativo o congiuntivo = finch\u00E8; si usa il congiuntivo se c'\u00E8 intenzione");
		lblDumDonecQuoad.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblQuotiensQuotienscumque = new JLabel("quotiens, quotienscumque + indicativo = tutte le volte che.");
		lblQuotiensQuotienscumque.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblEsprimonoLaCircostanza)
						.addComponent(lblInItalianoSono)
						.addComponent(lblCumIndicativo)
						.addComponent(lblCumAccompagnataDa)
						.addComponent(lblCumPrimumUbi)
						.addComponent(lblPostquamPosteaquamPostea)
						.addComponent(lblAntequamPriusquamAnte)
						.addComponent(lblSiUsaIl)
						.addComponent(lblDumIndicativo)
						.addComponent(lblDumDonecQuoad)
						.addComponent(lblQuotiensQuotienscumque)))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblEsprimonoLaCircostanza)
					.addGap(18)
					.addComponent(lblInItalianoSono)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCumIndicativo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCumAccompagnataDa)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCumPrimumUbi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblPostquamPosteaquamPostea)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAntequamPriusquamAnte)
					.addGap(18)
					.addComponent(lblSiUsaIl)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblDumIndicativo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblDumDonecQuoad)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblQuotiensQuotienscumque)
					.addContainerGap(12, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
