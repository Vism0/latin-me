package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class genitivo {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					genitivo window = new genitivo();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public genitivo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Genitivo");
		frame.setBounds(100, 100, 1736, 769);
		
		JLabel lblFunzioneDappartenenza = new JLabel("1. Funzione d\u2019Appartenenza:");
		lblFunzioneDappartenenza.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAGDi = new JLabel("a. G. di possesso: indica a chi appartiene una persona o una cosa.");
		lblAGDi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblBGEpesegetico = new JLabel("b. G. epesegetico: indica a quale categoria va riferito un nome (denominazione)");
		lblBGEpesegetico.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblCGDi = new JLabel("c. G. di qualit\u00E0: indica le qualit\u00E0 possedute da una persona.");
		lblCGDi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDGDi = new JLabel("d. G. di pertinenza: (di convenienza) indica la persona a cui tocca il dovere di compiere un\u2019azione.");
		lblDGDi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEGDet = new JLabel("e. G. d\u2019et\u00E0: esprime l\u2019et\u00E0 di una persona.");
		lblEGDet.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblFGDi = new JLabel("f. G. di misura: esprime una misura");
		lblFGDi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblFunzionePartitiva = new JLabel("2. Funzione Partitiva:");
		lblFunzionePartitiva.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAIndicaIl = new JLabel("a. Indica il tutto di cui viene presa in considerazione solo una parte.");
		lblAIndicaIl.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblFunzioneDi = new JLabel("3. Funzione di relazione: \u00E8 usato per specificare a quale realt\u00E0 va riferito un nome.");
		lblFunzioneDi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblADopoSostantivi = new JLabel("a. Dopo sostantivi che esprimono un\u2019azione.");
		lblADopoSostantivi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblBInDipendenza = new JLabel("b. In dipendenza da aggettivi che indicano:");
		lblBInDipendenza.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblIDesiderioPassione = new JLabel("i. Desiderio, passione, avversione: cupidus (desideroso), avidus (avido)");
		lblIDesiderioPassione.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblIiAbbondanzaMancanza = new JLabel("ii. Abbondanza, mancanza: plenus (pieno), vacuus (vuoto)");
		lblIiAbbondanzaMancanza.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblIiiConoscenzaEsperienza = new JLabel("iii. Conoscenza, esperienza, ricordo: peritus (esperto), imperitus (inesperto)");
		lblIiiConoscenzaEsperienza.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblIvSomiglianzaE = new JLabel("iv. Somiglianza e dissomiglianza: similis (simile), dissimilis (dissimile)");
		lblIvSomiglianzaE.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblCParticipiPresenti = new JLabel("c. Participi presenti usati come aggettivi: amans (amante), cupiens (desideroso)");
		lblCParticipiPresenti.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoCon = new JLabel("4. Genitivo con i verbi:");
		lblGenitivoCon.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAVerbiDi = new JLabel("a. Verbi di stima: aestimo, habeo, puto, duco, facio se accompagnati da un avverbio di quantit\u00E0 in genitivo assumono il valore di stimare, considerare.");
		lblAVerbiDi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblBVerbiDi = new JLabel("b. Verbi di memoria: mimini, reminiscor e obliviscor sono seguiti dal genitivo della persona o della cosa ricordate. ");
		lblBVerbiDi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblCVerbiDi = new JLabel("c. Verbi di accusa e condanna: i verbi che esprimono idea di accusare, condannare e assolvere sono seguiti dal genitivo indicante la colpa di cui si \u00E8 accusati o assolti.");
		lblCVerbiDi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDVerbiPienezza = new JLabel("d. Verbi pienezza abbondanza o privazione: alcuni verbi con l\u2019idea di completamento o abbondanza o di privazione sono seguiti dal genitivo.");
		lblDVerbiPienezza.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEGenitivoCon = new JLabel("e. Genitivo con interest o refert: interest o refert sono seguiti dal genitivo della persona a cui interessa una cosa. La cosa che importa \u00E8 espressa con un pronome neutro, con un infinito o con una subordinata soggettiva. ");
		lblEGenitivoCon.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblFGenitivoVerbi = new JLabel("f. Genitivo verbi impersonali: utilizzato con i verbi impersonali piget, puder paenitet taedet miseret per esprimere la cosa di cui ci si dispiace, pente ecc\u2026");
		lblFGenitivoVerbi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addComponent(lblAIndicaIl))
						.addComponent(lblFunzioneDappartenenza)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblBGEpesegetico)
								.addComponent(lblAGDi)
								.addComponent(lblCGDi)
								.addComponent(lblDGDi)
								.addComponent(lblEGDet)
								.addComponent(lblFGDi)))
						.addComponent(lblFunzionePartitiva)
						.addComponent(lblFunzioneDi)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblBInDipendenza)
								.addComponent(lblADopoSostantivi)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(10)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lblIiAbbondanzaMancanza)
										.addComponent(lblIDesiderioPassione)
										.addComponent(lblIiiConoscenzaEsperienza)
										.addComponent(lblIvSomiglianzaE)))
								.addComponent(lblCParticipiPresenti))
							.addGap(254))
						.addComponent(lblGenitivoCon)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblBVerbiDi)
								.addComponent(lblAVerbiDi)
								.addComponent(lblCVerbiDi)
								.addComponent(lblDVerbiPienezza)
								.addComponent(lblEGenitivoCon)
								.addComponent(lblFGenitivoVerbi))))
					.addContainerGap(146, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblFunzioneDappartenenza)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAGDi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblBGEpesegetico)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCGDi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblDGDi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEGDet)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblFGDi)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblFunzionePartitiva)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAIndicaIl)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblFunzioneDi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblADopoSostantivi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblBInDipendenza)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblIDesiderioPassione)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblIiAbbondanzaMancanza)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblIiiConoscenzaEsperienza)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblIvSomiglianzaE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCParticipiPresenti)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblGenitivoCon)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAVerbiDi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblBVerbiDi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCVerbiDi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblDVerbiPienezza)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEGenitivoCon)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblFGenitivoVerbi)
					.addContainerGap(139, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
