package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class perifrastica_passiva {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					perifrastica_passiva window = new perifrastica_passiva();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public perifrastica_passiva() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Perifrastica passiva");
		frame.setBounds(100, 100, 943, 258);
		
		JLabel lblCostituisceUnmezzo = new JLabel("Costituisce un \"mezzo\" che indica un'azione che si deve compiere,");
		lblCostituisceUnmezzo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNewLabel = new JLabel("e pu\u00F2 far parte di qualunque preposizione mantenendo lo stesso valore di \"obbligo\",\"dovere\" o \"necessit\u00E0\".");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNewLabel_1 = new JLabel("\u00C8 costituita dal gerundivo e dal verbo sum, coniugato in modo e tempo a seconda della preposizione di appartenenza.");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEssendoDiForma = new JLabel("Essendo di forma passiva, nelle traduzioni bisogna fare attenzione alle trasformazioni.");
		lblEssendoDiForma.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAncheIVerbi = new JLabel("Anche i verbi intransitivi possono avere la perifrastica passiva, ma solo nella forma impersonale.");
		lblAncheIVerbi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblCostituisceUnmezzo)
						.addComponent(lblNewLabel)
						.addComponent(lblNewLabel_1)
						.addComponent(lblEssendoDiForma)
						.addComponent(lblAncheIVerbi))
					.addContainerGap(34, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblCostituisceUnmezzo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNewLabel)
					.addGap(18)
					.addComponent(lblNewLabel_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEssendoDiForma)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAncheIVerbi)
					.addContainerGap(82, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
