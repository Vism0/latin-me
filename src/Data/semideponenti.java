package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class semideponenti {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					semideponenti window = new semideponenti();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public semideponenti() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Semideponenti");
		frame.setBounds(100, 100, 873, 354);
		
		JLabel lblIVerbiSi = new JLabel("i verbi si dicono semideponenti quando hanno una forma attiva nei tempi derivanti dal tema del presente");
		lblIVerbiSi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEUnaForma = new JLabel("e una forma passiva con valore attivo nei tempi derivanti dal tema del perfetto.");
		lblEUnaForma.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAudeoesAusus = new JLabel("audeo, -es, ausus sum, audere, \"osare\"");
		lblAudeoesAusus.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblGaudeoesGavisus = new JLabel("gaudeo, -es, gavisus sum, gaudere, \"gioire\"");
		lblGaudeoesGavisus.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblSoleoesSolitus = new JLabel("soleo, -es, solitus sum, solere, \"essere solito\"");
		lblSoleoesSolitus.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblFidoisFisus = new JLabel("fido, -is, fisus sum, fidere, \"fidarsi\"");
		lblFidoisFisus.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblConfidoisConfisus = new JLabel("confido, -is, confisus sum, confidere, \"confidare\"");
		lblConfidoisConfisus.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblDiffidoisDiffisus = new JLabel("diffido, -is, diffisus sum, diffidere, \"diffidare\"");
		lblDiffidoisDiffisus.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblFioFisFactus = new JLabel("fio, fis, factus sum, fieri, \"essere fatto\", \"divenire\", \"accadere\"(usato come passivo di facio)");
		lblFioFisFactus.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblEssiSono = new JLabel("Essi sono:");
		lblEssiSono.setFont(new Font("Tahoma", Font.BOLD, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblIVerbiSi)
						.addComponent(lblEUnaForma)
						.addComponent(lblEssiSono)
						.addComponent(lblFioFisFactus, GroupLayout.PREFERRED_SIZE, 706, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblDiffidoisDiffisus, GroupLayout.PREFERRED_SIZE, 335, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblConfidoisConfisus, GroupLayout.PREFERRED_SIZE, 366, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblFidoisFisus, GroupLayout.PREFERRED_SIZE, 279, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblSoleoesSolitus, GroupLayout.PREFERRED_SIZE, 351, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblGaudeoesGavisus, GroupLayout.PREFERRED_SIZE, 336, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblAudeoesAusus, GroupLayout.PREFERRED_SIZE, 306, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(178, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblIVerbiSi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEUnaForma)
					.addGap(5)
					.addComponent(lblEssiSono)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAudeoesAusus)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblGaudeoesGavisus)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblSoleoesSolitus)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblFidoisFisus)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblConfidoisConfisus)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblDiffidoisDiffisus)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblFioFisFactus)
					.addContainerGap(41, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
