package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class volo_nolo_malo {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					volo_nolo_malo window = new volo_nolo_malo();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public volo_nolo_malo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Volo, Nolo, Malo");
		frame.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 17));
		frame.setBounds(100, 100, 1410, 668);
		
		JLabel lblSonoVerbiAnomali = new JLabel("Sono verbi anomali o atematici perch\u00E8 in molte voci derivate dal tema del presente manca la vocale tematica. ");
		lblSonoVerbiAnomali.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblITempiDerivati = new JLabel("I tempi derivati dal perfetto e supino sono regolari Volo:");
		lblITempiDerivati.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblVoloPresentaUnalternanza = new JLabel("Volo presenta un\u2019alternanza vocalica nella radice tra o, e, u (vol-, vel-, vul-) a seconda del fonema che segue la l: ");
		lblVoloPresentaUnalternanza.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNewLabel = new JLabel("Troviamo infatti vol- prima di a, e, o, u (volam, voles, volo, volumus, ecc.); vel- prima di i o di altra l (velim, vellem ecc.); vul- prima di consonanti diverse dalla l (vulti ecc.).");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAltreParticolaritDi = new JLabel("Altre particolarit\u00E0 di questo verbo sono:");
		lblAltreParticolaritDi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblIlParticipio = new JLabel("\u2022 il participio presente volens, che ha valore di aggettivo e assume il significato di \u201Cbenigno\u201D;");
		lblIlParticipio.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblLeForme = new JLabel("\u2022 le forme di cortesia \u201Csi vis\u201D, \u201Csi vultis\u201D (se vuoi, se volete), che si trovano anche nella forma contratta sis, sultis;");
		lblLeForme.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblLaForma = new JLabel("\u2022 la forma visne? (vuoi?), che si pu\u00F2 trovare contratta in vin?;");
		lblLaForma.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblLeForme_1 = new JLabel("\u2022 le forme arcaiche volt e voltis per vult e vultis;");
		lblLeForme_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNolo = new JLabel("Nolo:");
		lblNolo.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblNasceDallaFusione = new JLabel("Nasce dalla fusione di non + volo; in alcune forme la negazione rimane staccata dal verbo: non vis, non vult, non vultis.");
		lblNasceDallaFusione.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblMalo = new JLabel("Malo: ");
		lblMalo.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblNasceDallaFusione_1 = new JLabel("Nasce dalla fusione di magis + volo (voglio di pi\u00F9 = preferisco).");
		lblNasceDallaFusione_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblRicorda = new JLabel("RICORDA");
		lblRicorda.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVoloNolo = new JLabel("\u2022 Volo, nolo e malo sono anche verbi difettivi, mancano cio\u00E8 di alcune forme: il gerundio, il gerundivo, il supino e i tempi da esso derivati. Inoltre soltanto nolo ha l\u2019imperativo.");
		lblVoloNolo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNonSono = new JLabel("\u2022 Non sono usate le forme passive.");
		lblNonSono.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNolinolite = new JLabel("\u2022 Noli/nolite + infinito presente rappresenta uno dei modi per esprimere un comando negativo alla seconda persona singolare o plurale.");
		lblNolinolite.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblSonoVerbiAnomali)
						.addComponent(lblITempiDerivati)
						.addComponent(lblVoloPresentaUnalternanza)
						.addComponent(lblNewLabel)
						.addComponent(lblAltreParticolaritDi)
						.addComponent(lblIlParticipio)
						.addComponent(lblLeForme)
						.addComponent(lblLaForma)
						.addComponent(lblLeForme_1)
						.addComponent(lblNolo)
						.addComponent(lblNasceDallaFusione)
						.addComponent(lblMalo)
						.addComponent(lblNasceDallaFusione_1)
						.addComponent(lblRicorda)
						.addComponent(lblVoloNolo)
						.addComponent(lblNonSono)
						.addComponent(lblNolinolite))
					.addContainerGap(56, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblSonoVerbiAnomali)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblITempiDerivati)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblVoloPresentaUnalternanza)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNewLabel)
					.addGap(18)
					.addComponent(lblAltreParticolaritDi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblIlParticipio)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblLeForme)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblLaForma)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblLeForme_1)
					.addGap(18)
					.addComponent(lblNolo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNasceDallaFusione)
					.addGap(18)
					.addComponent(lblMalo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNasceDallaFusione_1)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblRicorda)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblVoloNolo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNonSono)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNolinolite)
					.addContainerGap(22, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
