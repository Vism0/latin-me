package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class pronomi_indefinitivi {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					pronomi_indefinitivi window = new pronomi_indefinitivi();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public pronomi_indefinitivi() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Pronomi indefinitivi");
		frame.setBounds(100, 100, 852, 399);
		
		JLabel lblQuisqualcuno = new JLabel("1. quis \"qualcuno\"\u2013 qui \"qualche\"");
		lblQuisqualcuno.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAliquisqualcuno = new JLabel("2. aliquis \"qualcuno/ uno/ alcuno/ qualcheduno\" oppure \"uno importante/ qualcun altro\";");
		lblAliquisqualcuno.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblQuidamun = new JLabel("3. quidam \"un certo/ un tale/ uno\" quasi articolo indefinito ");
		lblQuidamun.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAlPlurale = new JLabel("\u2013 al plurale per indeterminatezza");
		lblAlPlurale.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblOppureuna = new JLabel("\u2013 oppure \"una specie di/ in un certo qual");
		lblOppureuna.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblQuisquamalcuno = new JLabel("4. quisquam \"alcuno/ qualcuno/ qualche\"");
		lblQuisquamalcuno.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblQuilibetqualsivoglia = new JLabel("5. quilibet \"qualsivoglia/ chiunque si sia/ qual piace/ qualunque\"");
		lblQuilibetqualsivoglia.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblQuivisqualsivoglia = new JLabel("6. quivis \"qualsivoglia/ qualunque/ qualsiasi/ ognuno/ chiunque\"");
		lblQuivisqualsivoglia.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblQuisquisqualunque = new JLabel("7. quisquis \"qualunque/ chiunque/ chicchessia\" - indefinito: \"qualcuno/ uno/ ciascuno\"");
		lblQuisquisqualunque.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblQuicumquequalunque = new JLabel("8. quicumque \"qualunque/ chiunque\" oppure \"la persona che\"\u2013 al neutro quodcumque");
		lblQuicumquequalunque.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAlPluralealcuni = new JLabel("Al plurale \"alcuni/ qualcuno/ certuni\" \u2013 aliqui \"qualche/un qualche/ uno\"");
		lblAlPluralealcuni.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addComponent(lblAlPluralealcuni))
						.addComponent(lblQuisqualcuno)
						.addComponent(lblAliquisqualcuno)
						.addComponent(lblQuidamun)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblOppureuna)
								.addComponent(lblAlPlurale)))
						.addComponent(lblQuisquamalcuno)
						.addComponent(lblQuilibetqualsivoglia)
						.addComponent(lblQuivisqualsivoglia)
						.addComponent(lblQuisquisqualunque)
						.addComponent(lblQuicumquequalunque))
					.addContainerGap(117, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblQuisqualcuno)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAliquisqualcuno)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAlPluralealcuni)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblQuidamun)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAlPlurale)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblOppureuna)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblQuisquamalcuno)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblQuilibetqualsivoglia)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblQuivisqualsivoglia)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblQuisquisqualunque)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblQuicumquequalunque)
					.addContainerGap(36, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
