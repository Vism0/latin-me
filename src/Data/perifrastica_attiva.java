package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class perifrastica_attiva {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					perifrastica_attiva window = new perifrastica_attiva();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public perifrastica_attiva() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Perifrastica attiva");
		frame.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 17));
		frame.setBounds(100, 100, 701, 449);
		
		JLabel lblLaPerifrasticaAttiva = new JLabel("La perifrastica attiva latina \u00E8 una forma verbale costituita dal gruppo :");
		lblLaPerifrasticaAttiva.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblParticipioFuturo = new JLabel("participio futuro + voci del verbo 'sum'.");
		lblParticipioFuturo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEssaIndicaUnazione = new JLabel("Essa indica un'azione :");
		lblEssaIndicaUnazione.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblImminente = new JLabel("- imminente ( essere sul punto di... / stare per )");
		lblImminente.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblIntenzionale = new JLabel("- intenzionale ( avere l'intenzione di... )");
		lblIntenzionale.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDiDestinazione = new JLabel("- di destinazione ( essere destinato a.. )");
		lblDiDestinazione.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblIlTempoDel = new JLabel("Il tempo del verbo italiano lo si prende dal tempo del verbo 'sum' latino.");
		lblIlTempoDel.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEsempio = new JLabel("ESEMPIO :");
		lblEsempio.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblCaesarVicturusErat = new JLabel("Caesar victurus erat proelium");
		lblCaesarVicturusErat.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblCesareEraSul = new JLabel("Cesare era sul punto di vincere la battaglia");
		lblCesareEraSul.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblLaPerifrasticaAttiva)
						.addComponent(lblParticipioFuturo)
						.addComponent(lblEssaIndicaUnazione)
						.addComponent(lblImminente)
						.addComponent(lblIntenzionale)
						.addComponent(lblDiDestinazione)
						.addComponent(lblIlTempoDel)
						.addComponent(lblEsempio)
						.addComponent(lblCesareEraSul)
						.addComponent(lblCaesarVicturusErat, GroupLayout.PREFERRED_SIZE, 238, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(85, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblLaPerifrasticaAttiva)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblParticipioFuturo)
					.addGap(18)
					.addComponent(lblEssaIndicaUnazione)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblImminente)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblIntenzionale)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblDiDestinazione)
					.addGap(18)
					.addComponent(lblIlTempoDel)
					.addGap(18)
					.addComponent(lblEsempio)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCaesarVicturusErat)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCesareEraSul)
					.addContainerGap(39, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
