package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class participio {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					participio window = new participio();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public participio() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Participio");
		frame.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 17));
		frame.setBounds(100, 100, 1653, 777);
		
		JLabel lblInLatinoVi = new JLabel("In latino vi sono tre tempi del participio:");
		lblInLatinoVi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblPresenteattivo = new JLabel("presente (attivo);");
		lblPresenteattivo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblPerfettoAttivi = new JLabel("perfetto attivi);");
		lblPerfettoAttivi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblFuturoattivo = new JLabel("futuro (attivo).");
		lblFuturoattivo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblQuelloPresenteHa = new JLabel("Quello presente ha significato attivo ed esprime la contemporaneit\u00E0 rispetto al verbo della reggente, con tutti i verbi (transitivi/intransitivi, attivi/deponenti).");
		lblQuelloPresenteHa.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSiRendeIn = new JLabel("Si rende in italiano nel seguente modo: ");
		lblSiRendeIn.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSeIlParticipio = new JLabel("Se il participio si trova in nominativo (sing. o plur.) si rende con il Gerundio semplice italiano.");
		lblSeIlParticipio.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblSeSiTrova = new JLabel("Se si trova in un altro caso si traduce con una proposizione relativa (Che...) oppure una temporale, causale, etc...");
		lblSeSiTrova.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblQuelloFuturoEsprime = new JLabel("Quello futuro esprime la posteriorit\u00E0 rispetto al verbo reggente, ha valore attivo e quindi \u00E8 presente nella coniugazione di tutti i verbi, eccetto i verbi difettivi di supino.");
		lblQuelloFuturoEsprime.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSiRendeIn_1 = new JLabel("Si rende in italiano nel seguente modo: ");
		lblSiRendeIn_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblColuiCheLodersta = new JLabel("Colui che loder\u00E0/sta per lodare/stava per lodare;");
		lblColuiCheLodersta.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblCheLoderstaPer = new JLabel("Che loder\u00E0/sta per lodare/stava per lodare");
		lblCheLoderstaPer.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblQuelloPerfettoEsprime = new JLabel("Quello perfetto esprime anteriorit\u00E0 e ha valore passivo.");
		lblQuelloPerfettoEsprime.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblVerboTransitivoAttivo = new JLabel("Verbo transitivo attivo (laudo, \"lodare\"). Ha il participio perfetto con valore passivo (laudatus, -a, -um).");
		lblVerboTransitivoAttivo.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblVerboIntransitivoAttivo = new JLabel("Verbo intransitivo attivo (venio, \"venire\"). Non ha il participio perfetto, tranne nei tempi derivati dal perfetto per formare la forma passiva impersonale (ventum est, \"si venne\").");
		lblVerboIntransitivoAttivo.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblVerboDeponenteTransitivo = new JLabel("Verbo deponente transitivo (hortor, \"esortare\"). Ha il participio perfetto con valore attivo (hortatus, -a, -um).");
		lblVerboDeponenteTransitivo.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblVerboDeponenteIntransitivo = new JLabel("Verbo deponente intransitivo (proficiscor, \"partire\"). Ha il participio perfetto con valore attivo (profectus, -a, -um).");
		lblVerboDeponenteIntransitivo.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblIlParticipioCongiunto = new JLabel("Il participio congiunto:");
		lblIlParticipioCongiunto.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblSiParlaDi = new JLabel("Si parla di participio congiunto quando il participio presente o perfetto di un verbo attivo, deponente o semideponente concorda in caso,");
		lblSiParlaDi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenereENumero = new JLabel("genere e numero con un elemento della proposizione reggente (sostantivo, pronome, ecc.).");
		lblGenereENumero.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNellaSuaFunzione = new JLabel("Nella sua funzione verbale corrisponde ad una proposizione subordinata implicita degli stessi tipi di quella dell'ablativo assoluto (temporale, causale, concessiva, finale) oppure alla protasi di un periodo ipotetico. ");
		lblNellaSuaFunzione.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVieneImpiegatoQuando = new JLabel("Viene impiegato quando l'ablativo assoluto non \u00E8 utilizzabile.");
		lblVieneImpiegatoQuando.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblInLatinoVi)
						.addComponent(lblPresenteattivo)
						.addComponent(lblPerfettoAttivi)
						.addComponent(lblFuturoattivo)
						.addComponent(lblQuelloPresenteHa)
						.addComponent(lblSiRendeIn)
						.addComponent(lblSeIlParticipio)
						.addComponent(lblSeSiTrova)
						.addComponent(lblQuelloFuturoEsprime)
						.addComponent(lblSiRendeIn_1)
						.addComponent(lblColuiCheLodersta)
						.addComponent(lblCheLoderstaPer)
						.addComponent(lblQuelloPerfettoEsprime)
						.addComponent(lblVerboTransitivoAttivo)
						.addComponent(lblVerboIntransitivoAttivo)
						.addComponent(lblVerboDeponenteTransitivo)
						.addComponent(lblVerboDeponenteIntransitivo)
						.addComponent(lblIlParticipioCongiunto)
						.addComponent(lblSiParlaDi)
						.addComponent(lblGenereENumero)
						.addComponent(lblNellaSuaFunzione)
						.addComponent(lblVieneImpiegatoQuando)))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblInLatinoVi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblPresenteattivo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblPerfettoAttivi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblFuturoattivo)
					.addGap(18)
					.addComponent(lblQuelloPresenteHa)
					.addGap(18)
					.addComponent(lblSiRendeIn)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblSeIlParticipio)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblSeSiTrova)
					.addGap(18)
					.addComponent(lblQuelloFuturoEsprime)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblSiRendeIn_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblColuiCheLodersta)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCheLoderstaPer)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblQuelloPerfettoEsprime)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblVerboTransitivoAttivo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblVerboIntransitivoAttivo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblVerboDeponenteTransitivo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblVerboDeponenteIntransitivo)
					.addGap(18)
					.addComponent(lblIlParticipioCongiunto)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblSiParlaDi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblGenereENumero)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblNellaSuaFunzione)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblVieneImpiegatoQuando)
					.addContainerGap(33, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
