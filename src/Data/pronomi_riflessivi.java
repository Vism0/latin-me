package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class pronomi_riflessivi {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					pronomi_riflessivi window = new pronomi_riflessivi();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public pronomi_riflessivi() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Pronomi riflessivi");
		frame.setBounds(100, 100, 1030, 399);
		
		JLabel lblIPronomiRiflessivi = new JLabel("I pronomi riflessivi corrispondono ai pronomi personali per la 1\u1D43 e 2\u1D43 persona, mentre per la 3\u1D43 si usano pronomi differenti.");
		lblIPronomiRiflessivi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblPersona = new JLabel("1\u1D43 PERSONA 2\u1D43 PERSONA 3\u1D43 PERSONA");
		lblPersona.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblSingolarePluraleSingolare = new JLabel("SINGOLARE PLURALE SINGOLARE PLURALE SINGOLARE");
		lblSingolarePluraleSingolare.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblNominativoEgoNos = new JLabel("NOMINATIVO Ego Nos Tu Vos ");
		lblNominativoEgoNos.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoMeiNostrum = new JLabel("GENITIVO Mei Nostrum, Nostri Tui Vestrum, Vestri Sui");
		lblGenitivoMeiNostrum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDativoMihiNobis = new JLabel("DATIVO Mihi Nobis Tibi Vobis Sibi");
		lblDativoMihiNobis.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccusativoMeNos = new JLabel("ACCUSATIVO Me Nos Te Vos Se");
		lblAccusativoMeNos.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVocativoTuVos = new JLabel("VOCATIVO Tu Vos ");
		lblVocativoTuVos.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblativoMeNobis = new JLabel("ABLATIVO Me Nobis Te Vobis Se");
		lblAblativoMeNobis.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblIPronomiRiflessivi)
						.addComponent(lblPersona)
						.addComponent(lblSingolarePluraleSingolare)
						.addComponent(lblNominativoEgoNos)
						.addComponent(lblGenitivoMeiNostrum)
						.addComponent(lblDativoMihiNobis)
						.addComponent(lblAccusativoMeNos)
						.addComponent(lblVocativoTuVos)
						.addComponent(lblAblativoMeNobis))
					.addContainerGap(38, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblIPronomiRiflessivi)
					.addGap(18)
					.addComponent(lblPersona)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblSingolarePluraleSingolare)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblNominativoEgoNos)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblGenitivoMeiNostrum)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblDativoMihiNobis)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAccusativoMeNos)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblVocativoTuVos)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAblativoMeNobis)
					.addContainerGap(59, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
