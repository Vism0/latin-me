package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class accusativo {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					accusativo window = new accusativo();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public accusativo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Accusativo");
		frame.setBounds(100, 100, 1471, 838);
		
		JLabel lblLaccusativoUn = new JLabel("L\u2019accusativo \u00E8 un caso attraverso il quale la lingua latina esprime differenti funzioni logiche, che corrispondono in italiano a diversi complementi, per\u00F2 pi\u00F9 di tutti al complemento oggetto.");
		lblLaccusativoUn.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVerbiConDoppio = new JLabel("VERBI CON DOPPIO ACCUSATIVO");
		lblVerbiConDoppio.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVerbiConAccusativo = new JLabel("VERBI CON ACCUSATIVO DELL\u2019OGGETTO E PREDICATIVO DELL\u2019OGGETTO");
		lblVerbiConAccusativo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVerbiAppellativi = new JLabel("1. Verbi appellativi: dico, voco, appello, nomino");
		lblVerbiAppellativi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVerbiEstimativi = new JLabel("2. Verbi estimativi: existimo, puto, iudico, duco, habeo");
		lblVerbiEstimativi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVerbiElettivi = new JLabel("3. Verbi elettivi: eligo,creo");
		lblVerbiElettivi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVerbiConAccusativo_1 = new JLabel("VERBI CON ACCUSATIVO DELL\u2019OGGETTO E DEL LUOGO");
		lblVerbiConAccusativo_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblQuestoCostrutto = new JLabel("questo costrutto \u00E8 proprio dei verbi che reggono l\u2019accusativo di luogo, quasi sempre composti dalle preposizioni circum e trans : traduco, circumduco,transmitto, transporto");
		lblQuestoCostrutto.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVerbiConAccusativo_2 = new JLabel("VERBI CON ACCUSATIVO DELLA PERSONA E DELLA COSA");
		lblVerbiConAccusativo_2.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDoceoE = new JLabel("1. DOCEO e CELO: accusativo della persona a cui si insegna + accusativo della cosa insegnata");
		lblDoceoE.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVerbarogandi = new JLabel("2. VERBA \u201Crogandi\u201D");
		lblVerbarogandi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblPoscoReposco = new JLabel("- POSCO, REPOSCO e INTERROGO: accusativo della persona a cui si chiede + accusativo della cosa chiesta");
		lblPoscoReposco.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblOroRogo = new JLabel("- ORO, ROGO, e INTERROGO: se cosa chiesta \u00E8 espressa da un pronome neutro, accusativo cosa chiesta + accusativo della persona a cui si chiede");
		lblOroRogo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblComplementoOggettoAccusativo = new JLabel("Complemento oggetto: Accusativo Semplice ");
		lblComplementoOggettoAccusativo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblComplementiDiMoto = new JLabel("Complementi di moto a luogo (ingresso): in + accusativo");
		lblComplementiDiMoto.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblPredicativoDelloggettoAccusativo = new JLabel("Predicativo dell\u2019oggetto: Accusativo Semplice ");
		lblPredicativoDelloggettoAccusativo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblComplementiDiMoto_1 = new JLabel("Complementi di moto a luogo: ad + accusativo");
		lblComplementiDiMoto_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblCausaEsternaObpropter = new JLabel("Causa esterna: ob/propter + accusativo ");
		lblCausaEsternaObpropter.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblComplementiDiMoto_2 = new JLabel("Complementi di moto per luogo: per + accusativo");
		lblComplementiDiMoto_2.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblComplementoDiFine = new JLabel("Complemento di fine: ad + accusativo ");
		lblComplementoDiFine.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblComplementoDiSvantaggio = new JLabel("Complemento di svantaggio (contro a): contra + accusativo");
		lblComplementoDiSvantaggio.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblComplementoPartitivoEex = new JLabel("Complemento partitivo: e/ex + ablativo inter + accusativo (pu\u00F2 essere espresso anche in altri modi) ");
		lblComplementoPartitivoEex.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblComplementoDiTempo = new JLabel("Complemento di tempo continuato: per + accusativo o - Accusativo Semplice");
		lblComplementoDiTempo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblComplementoDiMezzo = new JLabel("Complemento di mezzo (se espresso da persone): per + accusativo");
		lblComplementoDiMezzo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addComponent(lblQuestoCostrutto))
						.addComponent(lblLaccusativoUn)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblVerbiEstimativi)
								.addComponent(lblVerbiAppellativi)
								.addComponent(lblVerbiElettivi)))
						.addComponent(lblVerbiConAccusativo_1)
						.addComponent(lblVerbiConAccusativo_2)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblVerbarogandi)
								.addComponent(lblDoceoE)
								.addGroup(groupLayout.createSequentialGroup()
									.addGap(10)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lblOroRogo)
										.addComponent(lblPoscoReposco)))))
						.addComponent(lblComplementoOggettoAccusativo)
						.addComponent(lblComplementiDiMoto)
						.addComponent(lblPredicativoDelloggettoAccusativo)
						.addComponent(lblComplementiDiMoto_1)
						.addComponent(lblCausaEsternaObpropter)
						.addComponent(lblComplementiDiMoto_2)
						.addComponent(lblComplementoDiFine)
						.addComponent(lblComplementoDiSvantaggio)
						.addComponent(lblComplementoPartitivoEex)
						.addComponent(lblComplementoDiTempo)
						.addComponent(lblComplementoDiMezzo)
						.addComponent(lblVerbiConDoppio)
						.addComponent(lblVerbiConAccusativo))
					.addContainerGap(280, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblLaccusativoUn)
					.addGap(18)
					.addComponent(lblComplementoOggettoAccusativo)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblComplementiDiMoto)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblPredicativoDelloggettoAccusativo)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblComplementiDiMoto_1)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblCausaEsternaObpropter)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblComplementiDiMoto_2)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblComplementoDiFine)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblComplementoDiSvantaggio)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblComplementoPartitivoEex)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblComplementoDiTempo)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblComplementoDiMezzo)
					.addGap(18)
					.addComponent(lblVerbiConDoppio)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblVerbiConAccusativo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblVerbiAppellativi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblVerbiEstimativi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblVerbiElettivi)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblVerbiConAccusativo_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblQuestoCostrutto)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblVerbiConAccusativo_2)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblDoceoE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblVerbarogandi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblPoscoReposco)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblOroRogo)
					.addContainerGap(138, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
