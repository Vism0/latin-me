package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class gerundio {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					gerundio window = new gerundio();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public gerundio() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Gerundio");
		frame.setBounds(100, 100, 1148, 515);
		
		JLabel lblIlGerundioLatino = new JLabel("Il gerundio latino \u00E8 un nome verbale che \u00E8 usato come fosse la declinazione dell'infinito presente.");
		lblIlGerundioLatino.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSiFormaDal = new JLabel("Si forma dal tema del presente, ed \u00E8 posseduto da tutti i verbi, TRANSITIVI E INTRANSITIVI, ATTIVI E DEPONENTI. Ha sempre valore attivo.");
		lblSiFormaDal.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSiDeclinaAggiungendo = new JLabel("Si declina aggiungendo alle quattro coniugazioni rispettivamente A, E, E, IE, +ND e le terminazioni della 2\u00B0 declinazione nei dati casi.");
		lblSiDeclinaAggiungendo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblLaConiugazioneMista = new JLabel("La coniugazione mista in \u2013IO / IOR vuole \u2013IE della 4\u00B0 coniugazione. Il verbo sum e relativi composti non hanno il gerundio.");
		lblLaConiugazioneMista.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNeiVerbiDeponenti = new JLabel("Nei verbi deponenti ha forma attiva e significato attivo, con il supino in -UM, il participio presente e futuro e l\u2019infinito futuro");
		lblNeiVerbiDeponenti.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblUso = new JLabel("Uso:");
		lblUso.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblPerUtilizzareIl = new JLabel("Per utilizzare il gerundio latino, dobbiamo conoscere la funzione nominale dell'infinito.");
		lblPerUtilizzareIl.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAncheInItaliano = new JLabel("Anche in italiano l'infinito pu\u00F2 avere diverse funzioni grammaticali, pu\u00F2 essere soggetto, predicato nominale, od oggetto.");
		lblAncheInItaliano.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblComeSostantivoLinfinito = new JLabel("Come sostantivo, l\u2019infinito pu\u00F2 anche dover assumere la funzione di altri complementi; in questo caso l'infinito latino diventa gerundio.");
		lblComeSostantivoLinfinito.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoEsprimeLa = new JLabel("GENITIVO, esprime la funzione di SPECIFICAZIONE: DI CHI? DI CHE COSA?");
		lblGenitivoEsprimeLa.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDativoEsprimeLa = new JLabel("DATIVO, esprime la funzione di FINE: A, PER CHE FINE?");
		lblDativoEsprimeLa.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccusativoEsprimeLa = new JLabel("ACCUSATIVO, esprime la funzione di DIREZIONE O SCOPO (la funzione di oggetto viene svolta dall\u2019infinito): VERSO DOVE(spesso figurato)?");
		lblAccusativoEsprimeLa.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblativoEsprimeLa = new JLabel("ABLATIVO, esprime la funzione STRUMENTALE: PER MEZZO DI CHE?");
		lblAblativoEsprimeLa.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblIlGerundioLatino)
						.addComponent(lblSiFormaDal)
						.addComponent(lblSiDeclinaAggiungendo)
						.addComponent(lblLaConiugazioneMista)
						.addComponent(lblNeiVerbiDeponenti)
						.addComponent(lblUso)
						.addComponent(lblPerUtilizzareIl)
						.addComponent(lblAncheInItaliano)
						.addComponent(lblComeSostantivoLinfinito)
						.addComponent(lblGenitivoEsprimeLa)
						.addComponent(lblDativoEsprimeLa)
						.addComponent(lblAccusativoEsprimeLa)
						.addComponent(lblAblativoEsprimeLa)))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblIlGerundioLatino)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblSiFormaDal)
					.addGap(18)
					.addComponent(lblSiDeclinaAggiungendo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblLaConiugazioneMista)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNeiVerbiDeponenti)
					.addGap(18)
					.addComponent(lblUso)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblPerUtilizzareIl)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAncheInItaliano)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblComeSostantivoLinfinito)
					.addGap(18)
					.addComponent(lblGenitivoEsprimeLa)
					.addGap(18)
					.addComponent(lblDativoEsprimeLa)
					.addGap(18)
					.addComponent(lblAccusativoEsprimeLa)
					.addGap(18)
					.addComponent(lblAblativoEsprimeLa)
					.addContainerGap(39, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
