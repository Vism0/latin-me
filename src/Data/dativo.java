package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class dativo {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					dativo window = new dativo();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public dativo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Dativo");
		frame.setBounds(100, 100, 1154, 598);
		
		JLabel lblInItalianoIl = new JLabel("In italiano il dativo corrisponde principalmente al complemento di termine, ma pu\u00F2 tradurre anche i complementi di vantaggio, fine, relazione.");
		lblInItalianoIl.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblIlDoppioDativo = new JLabel("Il doppio dativo \u00E8 formato da due complementi in dativo:");
		lblIlDoppioDativo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblUnoIndicaUn = new JLabel("uno indica un effetto di vantaggio o danno, l\u2019altro la persona avvantaggiata o danneggiata.");
		lblUnoIndicaUn.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGeneralmenteSonoIntrodotti = new JLabel("Generalmente sono introdotti dal verbo sum e a volte dai verbi mitto, do, tribuno.");
		lblGeneralmenteSonoIntrodotti.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEsCaesarLegionem = new JLabel("Es: Caesar legionem misit auxilio sociis = Cesare mand\u00F2 una legione in aiuto agli alleati.");
		lblEsCaesarLegionem.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblIlVerboEsse = new JLabel("Il verbo esse nel dativo di possesso sostituisce il verbo habere. In questo costrutto il dativo indica il possessore.");
		lblIlVerboEsse.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEsCiceroniIn = new JLabel("Es: Ciceroni in Tusculano villa erat = Cicerone aveva una casa di campagna in quel di Tuscolo.");
		lblEsCiceroniIn.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblIlDativoDi = new JLabel("Il dativo di agente \u00E8 spesso usato insieme alla perifrastica passiva.");
		lblIlDativoDi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEsVobisHi = new JLabel("Es: Vobis hi coetus vitandi sunt = Voi dovete evitare queste compagnie.");
		lblEsVobisHi.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblAlcuniVeriCon = new JLabel("Alcuni veri con il dativo vanno tradotti in italiano con verbi transitivi.");
		lblAlcuniVeriCon.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEsBlandiriAlicui = new JLabel("Es: Blandiri alicui = Accarezzare uno");
		lblEsBlandiriAlicui.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblFavereAlicui = new JLabel("Favere alicui = Favorire uno");
		lblFavereAlicui.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblAlcuniVeriCon_1 = new JLabel("Alcuni veri con il dativo vanno tradotti in italiano con verbi intransitivi.");
		lblAlcuniVeriCon_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEsBeneDicere = new JLabel("Es: Bene dicere aliqui = Dire bene di uno");
		lblEsBeneDicere.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblIrasciAlicui = new JLabel("Irasci alicui = Adirarsi con uno");
		lblIrasciAlicui.setFont(new Font("Tahoma", Font.ITALIC, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblInItalianoIl)
						.addComponent(lblIlDoppioDativo)
						.addComponent(lblUnoIndicaUn)
						.addComponent(lblGeneralmenteSonoIntrodotti)
						.addComponent(lblEsCaesarLegionem)
						.addComponent(lblIlVerboEsse)
						.addComponent(lblEsCiceroniIn)
						.addComponent(lblIlDativoDi)
						.addComponent(lblEsVobisHi)
						.addComponent(lblAlcuniVeriCon)
						.addComponent(lblEsBlandiriAlicui)
						.addComponent(lblFavereAlicui)
						.addComponent(lblAlcuniVeriCon_1)
						.addComponent(lblEsBeneDicere)
						.addComponent(lblIrasciAlicui))
					.addContainerGap(168, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblInItalianoIl)
					.addGap(18)
					.addComponent(lblIlDoppioDativo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblUnoIndicaUn)
					.addGap(18)
					.addComponent(lblGeneralmenteSonoIntrodotti)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEsCaesarLegionem)
					.addGap(18)
					.addComponent(lblIlVerboEsse)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEsCiceroniIn)
					.addGap(18)
					.addComponent(lblIlDativoDi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEsVobisHi)
					.addGap(18)
					.addComponent(lblAlcuniVeriCon)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEsBlandiriAlicui)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblFavereAlicui)
					.addGap(18)
					.addComponent(lblAlcuniVeriCon_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEsBeneDicere)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblIrasciAlicui)
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
