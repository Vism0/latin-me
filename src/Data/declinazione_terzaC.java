package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class declinazione_terzaC {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					declinazione_terzaC window = new declinazione_terzaC();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public declinazione_terzaC() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("3 declinazione 3 gruppo");
		frame.setBounds(100, 100, 902, 465);
		
		JLabel lblFannoParteDel = new JLabel("Fanno parte del terzo gruppo solo sostantivi neutri con il nominativo in -e, -al,-ar");
		lblFannoParteDel.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEsempioMaremarisnmare = new JLabel("Esempio Mare,maris,n (mare)");
		lblEsempioMaremarisnmare.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblNomMareMaria = new JLabel("Nom mare mar-ia");
		lblNomMareMaria.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenMarisMarium = new JLabel("Gen mar-is mar-ium");
		lblGenMarisMarium.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDatMariMaribus = new JLabel("Dat mar-i mar-ibus");
		lblDatMariMaribus.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccMareMaria = new JLabel("Acc mare mar-ia");
		lblAccMareMaria.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVocMareMaria = new JLabel("Voc mare mar-ia");
		lblVocMareMaria.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblMariMaribus = new JLabel("Abl mar-i mar-ibus");
		lblAblMariMaribus.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblCiSonoDelle = new JLabel("Ci sono delle particolarit\u00E0:");
		lblCiSonoDelle.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblISostantivi = new JLabel("1) i sostantivi con tema diverso -iter,itineris -Iuppiter,Iovis -iecur,iecinoris (o iecoris) -femur,feminis (o femoris)");
		lblISostantivi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblISostantivi_1 = new JLabel("2) i sostantivi con declinazioni diverse e difettive -vas,vasis -vesper,vesperis -vis");
		lblISostantivi_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSostanziviCon = new JLabel("3) sostanzivi con uscite particolari -bos,bovis -sus,suis");
		lblSostanziviCon.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSostantiviSolo = new JLabel("4) sostantivi solo al plurale (pluralia tantum) -alpes,ium -moenia,ium -Optimates,ium -penates,ium");
		lblSostantiviSolo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblFannoParteDel)
						.addComponent(lblEsempioMaremarisnmare)
						.addComponent(lblNomMareMaria)
						.addComponent(lblGenMarisMarium)
						.addComponent(lblDatMariMaribus)
						.addComponent(lblAccMareMaria)
						.addComponent(lblVocMareMaria)
						.addComponent(lblAblMariMaribus)
						.addComponent(lblCiSonoDelle)
						.addComponent(lblISostantivi)
						.addComponent(lblISostantivi_1)
						.addComponent(lblSostanziviCon)
						.addComponent(lblSostantiviSolo))
					.addContainerGap(288, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblFannoParteDel)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblEsempioMaremarisnmare)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNomMareMaria)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblGenMarisMarium)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblDatMariMaribus)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAccMareMaria)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblVocMareMaria)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAblMariMaribus)
					.addGap(18)
					.addComponent(lblCiSonoDelle)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblISostantivi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblISostantivi_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblSostanziviCon)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblSostantiviSolo)
					.addContainerGap(233, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
