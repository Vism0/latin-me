package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class vocativo {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					vocativo window = new vocativo();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public vocativo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Vocativo");
		frame.setBounds(100, 100, 1316, 247);
		
		JLabel lblIlCasoVocativo = new JLabel("Il caso vocativo indica la persona o la cosa cui si rivolge un discorso diretto.");
		lblIlCasoVocativo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblMorfologicamenteIdentico = new JLabel("Morfologicamente \u00E8 identico al nominativo, eccezion fatta per il singolare dei nomi in -us della 2^ declinazione che al vocativo presentano la desinenza -e.");
		lblMorfologicamenteIdentico.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblInGenereIl = new JLabel("In genere il vocativo \u00E8 isolato dal resto della frase con l'apposizione di una o pi\u00F9 virgole; pu\u00F2 essere introdotto da interiezioni come o, heu, heus, eheu, pro, io, etc.");
		lblInGenereIl.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDiConseguenzaIl = new JLabel("di conseguenza, il nominativo, accusativo e vocativo dei nomi neutri sono omografi.");
		lblDiConseguenzaIl.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblIlCasoVocativo)
						.addComponent(lblMorfologicamenteIdentico)
						.addComponent(lblDiConseguenzaIl)
						.addComponent(lblInGenereIl))
					.addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblIlCasoVocativo)
					.addGap(18)
					.addComponent(lblMorfologicamenteIdentico)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblDiConseguenzaIl)
					.addGap(18)
					.addComponent(lblInGenereIl)
					.addContainerGap(71, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
