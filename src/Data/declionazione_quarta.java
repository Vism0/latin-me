package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class declionazione_quarta {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					declionazione_quarta window = new declionazione_quarta();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public declionazione_quarta() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Quarta declinazione");
		frame.setBounds(100, 100, 1176, 877);
		
		JLabel lblLaQuartaDeclinazione = new JLabel("La quarta declinazione comprende molti maschili (-us), pochi femminili (-us) (sopratutto di piante) e pochissimi neutri (-u) (come cornu e genu).");
		lblLaQuartaDeclinazione.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblMaschiliEFemminili = new JLabel("Maschili e femminili si declinano nello stesso modo.");
		lblMaschiliEFemminili.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblINeutriEscono = new JLabel("I neutri escono:");
		lblINeutriEscono.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblneiCasiRetti = new JLabel("-nei casi retti del singolare in \u2013u;");
		lblneiCasiRetti.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblneiCasiRetti_1 = new JLabel("-nei casi retti del plurale in \u2013ua.");
		lblneiCasiRetti_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblIlGenitivoSingolare = new JLabel("Il genitivo singolare \u00E8 per tutti in \u2013us.");
		lblIlGenitivoSingolare.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblmaschileSingolarePlurale = new JLabel("(maschile) SINGOLARE PLURALE");
		lblmaschileSingolarePlurale.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblNominativoCornuCornua = new JLabel("NOMINATIVO Corn-u Corn-ua");
		lblNominativoCornuCornua.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoCornusCornuum = new JLabel("GENITIVO Corn-us Corn-uum");
		lblGenitivoCornusCornuum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDativoCornuCornibus = new JLabel("DATIVO Corn-u Corn-ibus");
		lblDativoCornuCornibus.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccusativoCornuCornua = new JLabel("ACCUSATIVO Corn-u Corn-ua");
		lblAccusativoCornuCornua.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVocativoCornuCornua = new JLabel("VOCATIVO Corn-u Corn-ua");
		lblVocativoCornuCornua.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblativoCornuCornibus = new JLabel("ABLATIVO Corn-u Corn-ibus");
		lblAblativoCornuCornibus.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblneutroSingolarePlurale = new JLabel("(neutro) SINGOLARE PLURALE");
		lblneutroSingolarePlurale.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblNominativoFructusFructus = new JLabel("NOMINATIVO Fruct-us Fruct-us");
		lblNominativoFructusFructus.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoFructusFructuum = new JLabel("GENITIVO Fruct-us Fruct-uum");
		lblGenitivoFructusFructuum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDativoFructuiFructibus = new JLabel("DATIVO Fruct-ui Fruct-ibus");
		lblDativoFructuiFructibus.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccusativoFructumFructus = new JLabel("ACCUSATIVO Fruct-um Fruct-us");
		lblAccusativoFructumFructus.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVocativoFructusFructus = new JLabel("VOCATIVO Fruct-us Fruct-us");
		lblVocativoFructusFructus.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblativoFructuFructibus = new JLabel("ABLATIVO Fruct-u Fruct-ibus");
		lblAblativoFructuFructibus.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblParticolarita = new JLabel("PARTICOLARITA'");
		lblParticolarita.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblAlcuniMantengono = new JLabel("\u2022 Alcuni mantengono l\u2019antica terminazione in \u2013ubus nel dativo e ablativo plurale (arcus, lacus, specus e tribus);");
		lblAlcuniMantengono.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDueNomi = new JLabel("\u2022 Due nomi: \u201Cportus, veru\u201D hanno sia l\u2019uscita in \u2013ibus che in \u2013ubus;");
		lblDueNomi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAlcuniSostantivi = new JLabel("\u2022 Alcuni sostantivi oscillano tra la quarta e la seconda declinazionee sono:");
		lblAlcuniSostantivi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblquelliCheTerminano = new JLabel("-quelli che terminano in \u2013tus (senatus, i ed exercitus, i);");
		lblquelliCheTerminano.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblvariNomiDi = new JLabel("-vari nomi di piante (quercus con dat/abl plur. quercubus e gen. plur. ");
		lblvariNomiDi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblIlNome = new JLabel("\u2022 Il nome \u201Cdomus\u201D ha una declinazione particolare: al singolare fa domus, domus, domui o domo, domum, domus, domu o domo;");
		lblIlNome.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAlPluraleInvece = new JLabel("al plurale invece fa domus, domuum o domorum, domibus, domos, domus, domibus.");
		lblAlPluraleInvece.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblQuestoNomeChe = new JLabel("Questo nome, che vuol dire casa e patria, ha il locativo e fa \u201Cdomi\u201D.");
		lblQuestoNomeChe.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblFormaIlComplemento = new JLabel("Forma il complemento di luogo: moto a luogo e da luogo senza preposizione, per luogo con la preposizione. ");
		lblFormaIlComplemento.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEspressioniFamosiSono = new JLabel("Espressioni famosi sono \u201Cdomi bellique\u201D (in pace e in guerra) e \u201Cdomi militiaeque\u201D (in pace e in guerra).");
		lblEspressioniFamosiSono.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAlcuniSostantivi_1 = new JLabel("\u2022 Alcuni sostantivi sono usati generalmente solo in ablativo singolare e sono:");
		lblAlcuniSostantivi_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDuctu = new JLabel("- ductu + genitivo: sotto la guida di");
		lblDuctu.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblHortatu = new JLabel("- hortatu + genitivo: su esortazione di ");
		lblHortatu.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblIussu = new JLabel("- iussu + genitivo: ad ordine di ");
		lblIussu.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblRogatu = new JLabel("- rogatu + genitivo: su richiesta di. ");
		lblRogatu.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblLaQuartaDeclinazione)
						.addComponent(lblMaschiliEFemminili)
						.addComponent(lblINeutriEscono)
						.addComponent(lblneiCasiRetti)
						.addComponent(lblneiCasiRetti_1)
						.addComponent(lblIlGenitivoSingolare)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblmaschileSingolarePlurale)
								.addComponent(lblNominativoCornuCornua)
								.addComponent(lblGenitivoCornusCornuum)
								.addComponent(lblDativoCornuCornibus)
								.addComponent(lblAccusativoCornuCornua)
								.addComponent(lblVocativoCornuCornua)
								.addComponent(lblAblativoCornuCornibus))
							.addGap(68)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblAblativoFructuFructibus)
								.addComponent(lblVocativoFructusFructus)
								.addComponent(lblAccusativoFructumFructus)
								.addComponent(lblDativoFructuiFructibus)
								.addComponent(lblGenitivoFructusFructuum)
								.addComponent(lblNominativoFructusFructus)
								.addComponent(lblneutroSingolarePlurale)))
						.addComponent(lblParticolarita)
						.addComponent(lblAlcuniMantengono)
						.addComponent(lblDueNomi)
						.addComponent(lblAlcuniSostantivi)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblvariNomiDi)
								.addComponent(lblquelliCheTerminano)))
						.addComponent(lblIlNome)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblQuestoNomeChe)
								.addComponent(lblAlPluraleInvece)
								.addComponent(lblFormaIlComplemento)
								.addComponent(lblEspressioniFamosiSono)))
						.addComponent(lblAlcuniSostantivi_1)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblDuctu)
								.addComponent(lblHortatu))
							.addGap(28)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblRogatu)
								.addComponent(lblIussu))))
					.addContainerGap(84, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblLaQuartaDeclinazione)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblMaschiliEFemminili)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblINeutriEscono)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblneiCasiRetti)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblneiCasiRetti_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblIlGenitivoSingolare)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblmaschileSingolarePlurale)
						.addComponent(lblneutroSingolarePlurale))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNominativoCornuCornua)
						.addComponent(lblNominativoFructusFructus))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblGenitivoCornusCornuum)
						.addComponent(lblGenitivoFructusFructuum))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDativoCornuCornibus)
						.addComponent(lblDativoFructuiFructibus))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAccusativoCornuCornua)
						.addComponent(lblAccusativoFructumFructus))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblVocativoCornuCornua)
						.addComponent(lblVocativoFructusFructus))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAblativoCornuCornibus)
						.addComponent(lblAblativoFructuFructibus))
					.addGap(18)
					.addComponent(lblParticolarita)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAlcuniMantengono)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblDueNomi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAlcuniSostantivi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblquelliCheTerminano)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblvariNomiDi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblIlNome)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAlPluraleInvece)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblQuestoNomeChe)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblFormaIlComplemento)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEspressioniFamosiSono)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAlcuniSostantivi_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDuctu)
						.addComponent(lblIussu))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblHortatu)
						.addComponent(lblRogatu))
					.addContainerGap(43, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
