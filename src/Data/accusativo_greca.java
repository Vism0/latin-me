package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class accusativo_greca {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					accusativo_greca window = new accusativo_greca();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public accusativo_greca() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Accusativo alla greca");
		frame.setBounds(100, 100, 1320, 273);
		
		JLabel lblLaccusativoAllaGreca = new JLabel("L\u2019accusativo alla greca, detto anche accusativo di relazione, \u00E8 una costruzione che esprime mediante il caso accusativo il complemento di limitazione. ");
		lblLaccusativoAllaGreca.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblQuestoAccusativoCompleta = new JLabel("Questo accusativo completa o circoscrive il significato dell\u2019aggettivo a cui \u00E8 riferito, mentre quest\u2019ultimo concorda in genere, numero e caso col sostantivo cui si riferisce: ");
		lblQuestoAccusativoCompleta.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEsMulieresNudae = new JLabel("ES: Mulieres nudae brachia ac lacertos clementiam rogabant = le donne con le braccia e le spalle nude invocavano clemenza.");
		lblEsMulieresNudae.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblMoltoPiFrequente = new JLabel("Molto pi\u00F9 frequente \u00E8 il suo uso con il neutro pronominale:");
		lblMoltoPiFrequente.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEsIdTe = new JLabel("ES: Id te rogo = ti prego di questo ( = limitatamente/ relativamente a questo)");
		lblEsIdTe.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblEsHocGaudeo = new JLabel("ES: Hoc gaudeo = godo di questo ( = limitatamente/relativamente a questo) ");
		lblEsHocGaudeo.setFont(new Font("Tahoma", Font.ITALIC, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblLaccusativoAllaGreca)
						.addComponent(lblQuestoAccusativoCompleta)
						.addComponent(lblEsMulieresNudae)
						.addComponent(lblMoltoPiFrequente)
						.addComponent(lblEsIdTe)
						.addComponent(lblEsHocGaudeo))
					.addContainerGap(35, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblLaccusativoAllaGreca)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblQuestoAccusativoCompleta)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEsMulieresNudae)
					.addGap(18)
					.addComponent(lblMoltoPiFrequente)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEsIdTe)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEsHocGaudeo)
					.addContainerGap(119, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
