package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class supino {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					supino window = new supino();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public supino() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Supino");
		frame.setBounds(100, 100, 1085, 650);
		
		JLabel lblSupinoAttivo = new JLabel("SUPINO ATTIVO");
		lblSupinoAttivo.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblconconcon = new JLabel("1\u00B0con. 2\u00B0con. 3\u00B0con. 4\u00B0con.");
		lblconconcon.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAmatumDeletumMissum = new JLabel("Amatum Deletum Missum Auditum");
		lblAmatumDeletumMissum.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblDaNonConfondersi = new JLabel("Da non confondersi col participio passato,si usa in soli due casi:");
		lblDaNonConfondersi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblfraseFinale = new JLabel("1)frase finale ");
		lblfraseFinale.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblLoSiUsa = new JLabel("Lo si usa in concordanza con un verbo di movimento ed esprime un fine,uno scopo:");
		lblLoSiUsa.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEsempioCaesarMilites = new JLabel("Esempio: Caesar milites misit pontem deletum. ");
		lblEsempioCaesarMilites.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblCesareInviI = new JLabel("Cesare invi\u00F2 i soldati A DISTRUGGERE il ponte.");
		lblCesareInviI.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSiTraduceNormalmente = new JLabel("Si traduce normalmente con l'infinito presente attivo.");
		lblSiTraduceNormalmente.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblinfinitoFuturoPassivo = new JLabel("2)infinito futuro passivo ");
		lblinfinitoFuturoPassivo.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblSiUsaPer = new JLabel("Si usa per formare l'infinito futuro passivo aggiungendo il termine iri:");
		lblSiUsaPer.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAmatumIriDeletum = new JLabel("Amatum iri, Deletum iri ecc....");
		lblAmatumIriDeletum.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblSupinoPassivo = new JLabel("SUPINO PASSIVO ");
		lblSupinoPassivo.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblconconcon_1 = new JLabel("1\u00B0con. 2\u00B0con. 3\u00B0con. 4\u00B0con.");
		lblconconcon_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAmatuDeletuMissu = new JLabel("Amatu Deletu Missu Auditu");
		lblAmatuDeletuMissu.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblDiUsoEstremamente = new JLabel("Di uso estremamente raro,Si traduce con l'infinito presente attivo o con la preposizione a +infinito infinito presente in forma riflessiva.");
		lblDiUsoEstremamente.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEsempisicOpus = new JLabel("Esempi: 1)Sic opus est dictu.");
		lblEsempisicOpus.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblBisognaDireCos = new JLabel("Bisogna DIRE cos\u00EC.");
		lblBisognaDireCos.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblmalaVerbaNon = new JLabel("2)Mala verba non nitida sunt auditu.");
		lblmalaVerbaNon.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblLeBrutteParole = new JLabel("Le brutte parole non sono belle A SENTIRSI.");
		lblLeBrutteParole.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addContainerGap()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblSupinoAttivo)
								.addComponent(lblconconcon)
								.addComponent(lblDaNonConfondersi)
								.addComponent(lblfraseFinale)
								.addComponent(lblLoSiUsa)
								.addComponent(lblEsempioCaesarMilites)
								.addComponent(lblCesareInviI)
								.addComponent(lblSiTraduceNormalmente)
								.addComponent(lblinfinitoFuturoPassivo)
								.addComponent(lblSiUsaPer)
								.addComponent(lblSupinoPassivo)
								.addComponent(lblconconcon_1)
								.addComponent(lblDiUsoEstremamente)
								.addGroup(groupLayout.createSequentialGroup()
									.addComponent(lblEsempisicOpus)
									.addPreferredGap(ComponentPlacement.RELATED)
									.addComponent(lblBisognaDireCos))
								.addComponent(lblAmatumDeletumMissum, GroupLayout.PREFERRED_SIZE, 269, GroupLayout.PREFERRED_SIZE)
								.addGroup(groupLayout.createParallelGroup(Alignment.TRAILING, false)
									.addComponent(lblAmatumIriDeletum, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
									.addComponent(lblAmatuDeletuMissu, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(78)
							.addComponent(lblmalaVerbaNon)
							.addPreferredGap(ComponentPlacement.RELATED)
							.addComponent(lblLeBrutteParole)))
					.addContainerGap(153, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblSupinoAttivo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblconconcon)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAmatumDeletumMissum)
					.addGap(18)
					.addComponent(lblDaNonConfondersi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblfraseFinale)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblLoSiUsa)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEsempioCaesarMilites)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCesareInviI)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblSiTraduceNormalmente)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblinfinitoFuturoPassivo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblSiUsaPer)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAmatumIriDeletum)
					.addGap(18)
					.addComponent(lblSupinoPassivo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblconconcon_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAmatuDeletuMissu)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblDiUsoEstremamente)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblEsempisicOpus)
						.addComponent(lblBisognaDireCos))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblLeBrutteParole)
						.addComponent(lblmalaVerbaNon))
					.addContainerGap(48, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
