package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class finale {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					finale window = new finale();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public finale() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Finale");
		frame.setBounds(100, 100, 1311, 317);
		
		JLabel lblLaSubordinataFinale = new JLabel("La subordinata finale esplicita in latino pu\u00F2 essere espressa in 3 modi");
		lblLaSubordinataFinale.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblconUtO = new JLabel("1)con ut o ne (negazione) + il congiuntivo (si utilizza quello presente se dipendente da un tempo principale o quello imperfetto se dipendente da un tempo storico)");
		lblconUtO.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblconQuo = new JLabel("2)con quo + il congiuntivo (solitamente se nella reggente \u00E8 presente un comparativo)");
		lblconQuo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblconQuiquaequod = new JLabel("3)con qui/quae/quod + il congiuntivo (relativa impropria, non sempre ha valore di finale)");
		lblconQuiquaequod.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblLaFinaleImplicita = new JLabel("La finale implicita pu\u00F2 essere espressa con");
		lblLaFinaleImplicita.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblilParticipioFuturo = new JLabel("1)il participio futuro");
		lblilParticipioFuturo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblsupinoInDipendenza = new JLabel("2)supino in dipendenza da verbi di moto");
		lblsupinoInDipendenza.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblilGenitivoDel = new JLabel("3)il genitivo del gerundio o gerundivo + causa/gratia");
		lblilGenitivoDel.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblLaSubordinataFinale)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblconQuo)
								.addComponent(lblconUtO)
								.addComponent(lblconQuiquaequod)))
						.addComponent(lblLaFinaleImplicita)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblsupinoInDipendenza)
								.addComponent(lblilParticipioFuturo)
								.addComponent(lblilGenitivoDel))))
					.addContainerGap(90, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblLaSubordinataFinale)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblconUtO)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblconQuo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblconQuiquaequod)
					.addGap(18)
					.addComponent(lblLaFinaleImplicita)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblilParticipioFuturo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblsupinoInDipendenza)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblilGenitivoDel)
					.addContainerGap(84, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
