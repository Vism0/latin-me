package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class pronomi_dimostrativi {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					pronomi_dimostrativi window = new pronomi_dimostrativi();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public pronomi_dimostrativi() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Pronomi Dimostrativi");
		frame.setBounds(100, 100, 760, 743);
		
		JLabel lblHicHaecHoc = new JLabel("hic, haec, hoc \"questo\" (vicino a chi parla)");
		lblHicHaecHoc.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblSingolare = new JLabel("Singolare ");
		lblSingolare.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNominativoHicHaec = new JLabel("NOMINATIVO Hic Haec Hoc");
		lblNominativoHicHaec.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoHuiusHuius = new JLabel("GENITIVO Huius Huius Huius");
		lblGenitivoHuiusHuius.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDativoHiucHuic = new JLabel("DATIVO Hiuc Huic Huic");
		lblDativoHiucHuic.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccusativoHuncHanc = new JLabel("ACCUSATIVO Hunc Hanc Hoc");
		lblAccusativoHuncHanc.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblativoHocHac = new JLabel("ABLATIVO Hoc Hac Hoc");
		lblAblativoHocHac.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblPlurale = new JLabel("Plurale");
		lblPlurale.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNominativoHiHae = new JLabel("NOMINATIVO Hi Hae Haec");
		lblNominativoHiHae.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoHorumHarum = new JLabel("GENITIVO Horum Harum Horum");
		lblGenitivoHorumHarum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDativoHisHis = new JLabel("DATIVO His His His");
		lblDativoHisHis.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccusativoHosHas = new JLabel("ACCUSATIVO Hos Has Haec");
		lblAccusativoHosHas.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblativoHisHis = new JLabel("ABLATIVO His His His");
		lblAblativoHisHis.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblIsteIstaIstud = new JLabel("iste, ista, istud \"codesto\" (oggetto vicino a chi ascolta)");
		lblIsteIstaIstud.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblSingolare_1 = new JLabel("Singolare");
		lblSingolare_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNominativoIsteIsta = new JLabel("NOMINATIVO Iste Ista Istud");
		lblNominativoIsteIsta.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoIstiusIstius = new JLabel("GENITIVO Istius Istius Istius");
		lblGenitivoIstiusIstius.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDativoIstiIsti = new JLabel("DATIVO Isti Isti Isti");
		lblDativoIstiIsti.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccusativoIstumIstam = new JLabel("ACCUSATIVO Istum Istam Istud");
		lblAccusativoIstumIstam.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblativoIstoIsta = new JLabel("ABLATIVO Isto Ista Isto");
		lblAblativoIstoIsta.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblPlurale_1 = new JLabel("Plurale");
		lblPlurale_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNominativoIstiIstae = new JLabel("NOMINATIVO Isti Istae Ista");
		lblNominativoIstiIstae.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoIstorumIstarum = new JLabel("GENITIVO Istorum Istarum Istorum");
		lblGenitivoIstorumIstarum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDativoIstisIstis = new JLabel("DATIVO Istis Istis Istis");
		lblDativoIstisIstis.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccusativoIstosIstas = new JLabel("ACCUSATIVO Istos Istas Ista");
		lblAccusativoIstosIstas.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblativoIstisIstis = new JLabel("ABLATIVO Istis Istis Istis");
		lblAblativoIstisIstis.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblIlleIllaIllud = new JLabel("ille, illa, illud \"quello\" (oggetto lontano da chi parla e da chi ascolta)");
		lblIlleIllaIllud.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblSingolare_2 = new JLabel("Singolare");
		lblSingolare_2.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNominativoIlleIlla = new JLabel("NOMINATIVO Ille Illa Illud");
		lblNominativoIlleIlla.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoIlliusIllius = new JLabel("GENITIVO Illius Illius Illius");
		lblGenitivoIlliusIllius.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDativoIlliIlli = new JLabel("DATIVO Illi Illi Illi");
		lblDativoIlliIlli.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccusativoIllumIllam = new JLabel("ACCUSATIVO Illum Illam Illud");
		lblAccusativoIllumIllam.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblativoIlloIlla = new JLabel("ABLATIVO Illo Illa Illo");
		lblAblativoIlloIlla.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblPlurale_2 = new JLabel("Plurale");
		lblPlurale_2.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNominativoIlliIllae = new JLabel("NOMINATIVO Illi Illae Illa");
		lblNominativoIlliIllae.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoIllorumIllarum = new JLabel("GENITIVO Illorum Illarum Illorum");
		lblGenitivoIllorumIllarum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDativoIllisIllis = new JLabel("DATIVO Illis Illis Illis");
		lblDativoIllisIllis.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccusativoIllosIllas = new JLabel("ACCUSATIVO Illos Illas Illa");
		lblAccusativoIllosIllas.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblativoIllisIllis = new JLabel("ABLATIVO Illis Illis Illis");
		lblAblativoIllisIllis.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblSingolare)
								.addComponent(lblNominativoHicHaec)
								.addComponent(lblGenitivoHuiusHuius)
								.addComponent(lblDativoHiucHuic)
								.addComponent(lblAccusativoHuncHanc)
								.addComponent(lblAblativoHocHac))
							.addGap(55)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblAblativoHisHis)
								.addComponent(lblPlurale)
								.addComponent(lblNominativoHiHae, GroupLayout.PREFERRED_SIZE, 219, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblDativoHisHis, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblAccusativoHosHas, GroupLayout.DEFAULT_SIZE, 334, Short.MAX_VALUE)
								.addComponent(lblGenitivoHorumHarum, GroupLayout.PREFERRED_SIZE, 248, GroupLayout.PREFERRED_SIZE)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblNominativoIsteIsta)
								.addComponent(lblGenitivoIstiusIstius)
								.addComponent(lblDativoIstiIsti)
								.addComponent(lblAccusativoIstumIstam)
								.addComponent(lblSingolare_1)
								.addComponent(lblAblativoIstoIsta))
							.addGap(41)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblAblativoIstisIstis)
								.addComponent(lblDativoIstisIstis)
								.addComponent(lblPlurale_1)
								.addComponent(lblNominativoIstiIstae, GroupLayout.PREFERRED_SIZE, 216, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblGenitivoIstorumIstarum, GroupLayout.PREFERRED_SIZE, 284, GroupLayout.PREFERRED_SIZE)
								.addComponent(lblAccusativoIstosIstas, GroupLayout.PREFERRED_SIZE, 233, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblSingolare_2)
								.addComponent(lblNominativoIlleIlla)
								.addComponent(lblGenitivoIlliusIllius)
								.addComponent(lblDativoIlliIlli)
								.addComponent(lblAccusativoIllumIllam)
								.addComponent(lblAblativoIlloIlla))
							.addGap(53)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblAblativoIllisIllis)
								.addComponent(lblAccusativoIllosIllas)
								.addComponent(lblDativoIllisIllis)
								.addComponent(lblNominativoIlliIllae)
								.addComponent(lblPlurale_2)
								.addComponent(lblGenitivoIllorumIllarum, GroupLayout.PREFERRED_SIZE, 255, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED))
						.addComponent(lblHicHaecHoc, GroupLayout.PREFERRED_SIZE, 356, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblIsteIstaIstud, GroupLayout.DEFAULT_SIZE, 434, Short.MAX_VALUE)
						.addComponent(lblIlleIllaIllud, GroupLayout.PREFERRED_SIZE, 521, GroupLayout.PREFERRED_SIZE))
					.addGap(128))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblHicHaecHoc)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSingolare)
						.addComponent(lblPlurale))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNominativoHicHaec)
						.addComponent(lblNominativoHiHae))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblGenitivoHuiusHuius)
						.addComponent(lblGenitivoHorumHarum))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDativoHiucHuic)
						.addComponent(lblDativoHisHis))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAccusativoHuncHanc)
						.addComponent(lblAccusativoHosHas))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAblativoHocHac)
						.addComponent(lblAblativoHisHis))
					.addGap(18)
					.addComponent(lblIsteIstaIstud)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSingolare_1)
						.addComponent(lblPlurale_1))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNominativoIsteIsta)
						.addComponent(lblNominativoIstiIstae))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblGenitivoIstiusIstius)
						.addComponent(lblGenitivoIstorumIstarum))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDativoIstiIsti)
						.addComponent(lblDativoIstisIstis))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAccusativoIstumIstam)
						.addComponent(lblAccusativoIstosIstas))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAblativoIstoIsta)
						.addComponent(lblAblativoIstisIstis))
					.addGap(18)
					.addComponent(lblIlleIllaIllud)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSingolare_2)
						.addComponent(lblPlurale_2))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNominativoIlleIlla)
						.addComponent(lblNominativoIlliIllae))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblGenitivoIlliusIllius)
						.addComponent(lblGenitivoIllorumIllarum))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDativoIlliIlli)
						.addComponent(lblDativoIllisIllis))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAccusativoIllumIllam)
						.addComponent(lblAccusativoIllosIllas))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAblativoIlloIlla)
						.addComponent(lblAblativoIllisIllis))
					.addContainerGap(108, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
