package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class imperativo {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					imperativo window = new imperativo();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public imperativo() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Imperativo");
		frame.getContentPane().setFont(new Font("Tahoma", Font.PLAIN, 17));
		frame.setBounds(100, 100, 1547, 1035);
		
		JLabel lblLimperativoUn = new JLabel("L\u2019imperativo \u00E8 un modo finito che esprime l\u2019idea di comando. Ha due tempi:");
		lblLimperativoUn.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblPresenteChe = new JLabel("- presente, che esprime un ordine la cui esecuzione deve essere immediata ed ha solo due persone: per le altre si usa il congiuntivo;");
		lblPresenteChe.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblfuturoCheEsprime = new JLabel("-futuro, che esprime un ordine o un ammonimento solenne destinato ad essere attuato nel futuro o con continuit\u00E0 ed ha la 2\u00AA e la 3\u00AA persona singolare e plurale.");
		lblfuturoCheEsprime.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAlPassivoLe = new JLabel("Al passivo, le desinenze dell\u2019imperativo presente sono uguali a quelle dell\u2019indicativo (re, mini), ma, evidentemente, si trovano usate solo nei verbi deponenti:");
		lblAlPassivoLe.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblMiserreAbbi = new JLabel("miser\u0113re = abbi compassione; fat\u0113re = confessa; loqu\u0115re = parla;");
		lblMiserreAbbi.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblLimperativoSiForma = new JLabel("L\u2019imperativo si forma aggiungendo le uscite al tema del presente:");
		lblLimperativoSiForma.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblLaSecondaPersona = new JLabel("la seconda persona singolare mostra il puro tema, senza desinenza, solamente con la vocale congiuntiva; la seconda plurale ha per desinenza te che viene affissa al tema mediante la vocale congiuntiva.");
		lblLaSecondaPersona.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblLaSecondaE = new JLabel("La seconda e terza persona singolare dell\u2019imperativo futuro si ottengono aggiungendo al tema, dopo la vocale congiuntiva, la desinenza to, che \u00E8 un antico ablativo avverbiale (= d\u2019ora in poi).");
		lblLaSecondaE.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNellaPlurale = new JLabel("Nella 2\u00AA plurale al suffisso to si \u00E8 aggiunta per analogia col presente la desinenza te.");
		lblNellaPlurale.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblLaP = new JLabel("La 3\u00AA p. plurale, invece, \u00E8 formata sul presente indicativo (laudanto \u2190 laudant). ");
		lblLaP.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblPresente = new JLabel("Presente");
		lblPresente.setFont(new Font("Tahoma", Font.BOLD, 11));
		
		JLabel lblLaudLoda = new JLabel("laud- \u0101 loda");
		lblLaudLoda.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblLaudteLodate = new JLabel("laud- \u0101te lodate");
		lblLaudteLodate.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblMonAvvisa = new JLabel("mon- \u0113 avvisa");
		lblMonAvvisa.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblMonteAvvisate = new JLabel("mon- \u0113te avvisate");
		lblMonteAvvisate.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblLegLeggi = new JLabel("leg- \u0115 leggi");
		lblLegLeggi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblLegteLeggete = new JLabel("leg- \u012Dte leggete");
		lblLegteLeggete.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAudOdi = new JLabel("aud- \u012B odi");
		lblAudOdi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAudteUdite = new JLabel("aud- \u012Bte udite");
		lblAudteUdite.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblFuturo = new JLabel("Futuro");
		lblFuturo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblLaudtoLoderai = new JLabel("laud- \u0101to loderai");
		lblLaudtoLoderai.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lbltoLoder = new JLabel("\u0101to loder\u00E0");
		lbltoLoder.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAtteLoderete = new JLabel("at\u014Dte loderete");
		lblAtteLoderete.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblntoLoderanno = new JLabel("\u0101nto loderanno");
		lblntoLoderanno.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblMontoAvviserai = new JLabel("mon- \u0113to avviserai");
		lblMontoAvviserai.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lbltoAvviser = new JLabel("\u0113to avviser\u00E0");
		lbltoAvviser.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEtteAvviserete = new JLabel("et\u014Dte avviserete");
		lblEtteAvviserete.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblntoAvviseranno = new JLabel("\u0113nto avviseranno");
		lblntoAvviseranno.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblLegtoLeggerai = new JLabel("leg- \u012Dto leggerai");
		lblLegtoLeggerai.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lbltoLegger = new JLabel("\u012Dto legger\u00E0");
		lbltoLegger.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblItteLeggerete = new JLabel("it\u014Dte leggerete");
		lblItteLeggerete.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblntoLeggeranno = new JLabel("\u016Bnto leggeranno ");
		lblntoLeggeranno.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAudtoUdrai = new JLabel("aud- \u012Bto udrai");
		lblAudtoUdrai.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lbltoUdr = new JLabel("\u012Bto udr\u00E0");
		lbltoUdr.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblItteUdrete = new JLabel("it\u014Dte udrete");
		lblItteUdrete.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblIntoUdranno = new JLabel("i\u016Bnto udranno ");
		lblIntoUdranno.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblOsservazione = new JLabel("OSSERVAZIONE");
		lblOsservazione.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblInPraticaLa = new JLabel("In pratica la 2\u00AA persona sing. dell\u2019imperativo presente si ottiene togliendo all\u2019infinito la desinenza");
		lblInPraticaLa.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblrelaud = new JLabel("-re (laud\u0101 \u2013 re, mon\u0113\u2013 re, leg\u0115 \u2013re, aud\u012B \u2013re).");
		lblrelaud.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblParticolarit = new JLabel("PARTICOLARIT\u00C0");
		lblParticolarit.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblIVerbiDico = new JLabel("I verbi dico, duco, facio e fero hanno la seconda persona singolare dell\u2019imperativo presente tronca, cio\u00E8 senza la \u0115: dic, duc, fac, fer.");
		lblIVerbiDico.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblIlComandoNegativo = new JLabel("Il comando negativo si esprime:");
		lblIlComandoNegativo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblConNeE = new JLabel("con ne e il congiuntivo presente se \u00E8 rivolto alla prima e terza persona, cio\u00E8 se nega un congiuntivo esortativo");
		lblConNeE.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblConNeE_1 = new JLabel("con ne e il congiuntivo perfetto, se il comando \u00E8 rivolto alla seconda persona (sing. o plurale), cio\u00E8 per le forme propriamente imperative");
		lblConNeE_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNellePrescrizioniDi = new JLabel("Nelle prescrizioni di leggi con ne e la II o III persona singolare o plurale dell\u2019imperativo futuro");
		lblNellePrescrizioniDi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addComponent(lblMiserreAbbi))
						.addComponent(lblLimperativoUn)
						.addComponent(lblPresenteChe)
						.addComponent(lblfuturoCheEsprime)
						.addComponent(lblAlPassivoLe)
						.addComponent(lblLimperativoSiForma)
						.addComponent(lblLaSecondaPersona)
						.addComponent(lblLaSecondaE)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblLaP)
								.addComponent(lblNellaPlurale)))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblPresente)
								.addComponent(lblLaudLoda)
								.addComponent(lblLaudteLodate)
								.addComponent(lblMonAvvisa)
								.addComponent(lblMonteAvvisate)
								.addComponent(lblLegLeggi)
								.addComponent(lblLegteLeggete)
								.addComponent(lblAudOdi)
								.addComponent(lblAudteUdite))
							.addGap(67)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblntoAvviseranno)
								.addComponent(lblEtteAvviserete)
								.addComponent(lbltoAvviser)
								.addComponent(lblMontoAvviserai)
								.addComponent(lblntoLoderanno)
								.addComponent(lblAtteLoderete)
								.addComponent(lbltoLoder)
								.addComponent(lblLaudtoLoderai)
								.addComponent(lblFuturo)
								.addComponent(lblLegtoLeggerai)
								.addComponent(lbltoLegger)
								.addComponent(lblItteLeggerete)
								.addComponent(lblntoLeggeranno)
								.addComponent(lblAudtoUdrai)
								.addComponent(lbltoUdr)
								.addComponent(lblItteUdrete)
								.addComponent(lblIntoUdranno)))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblOsservazione)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(lblInPraticaLa)
							.addGap(18)
							.addComponent(lblrelaud))
						.addGroup(groupLayout.createSequentialGroup()
							.addComponent(lblParticolarit)
							.addPreferredGap(ComponentPlacement.UNRELATED)
							.addComponent(lblIVerbiDico))
						.addComponent(lblIlComandoNegativo)
						.addComponent(lblConNeE)
						.addComponent(lblConNeE_1)
						.addComponent(lblNellePrescrizioniDi))
					.addContainerGap(181, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblLimperativoUn)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblPresenteChe)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblfuturoCheEsprime)
					.addGap(18)
					.addComponent(lblAlPassivoLe)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblMiserreAbbi)
					.addGap(18)
					.addComponent(lblLimperativoSiForma)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblLaSecondaPersona)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblLaSecondaE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNellaPlurale)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblLaP)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblPresente)
						.addComponent(lblFuturo))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblLaudLoda)
						.addComponent(lblLaudtoLoderai))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblLaudteLodate)
						.addComponent(lbltoLoder))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblMonAvvisa)
						.addComponent(lblAtteLoderete))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblMonteAvvisate)
						.addComponent(lblntoLoderanno))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblLegLeggi)
						.addComponent(lblMontoAvviserai))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblLegteLeggete)
						.addComponent(lbltoAvviser))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAudOdi)
						.addComponent(lblEtteAvviserete))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAudteUdite)
						.addComponent(lblntoAvviseranno))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblLegtoLeggerai)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lbltoLegger)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblItteLeggerete)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblntoLeggeranno)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAudtoUdrai)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lbltoUdr)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblItteUdrete)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblIntoUdranno)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblOsservazione)
						.addComponent(lblInPraticaLa)
						.addComponent(lblrelaud))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblParticolarit)
						.addComponent(lblIVerbiDico))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblIlComandoNegativo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblConNeE)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblConNeE_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblNellePrescrizioniDi)
					.addContainerGap(64, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
