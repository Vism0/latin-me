package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class pluralia_tantum {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					pluralia_tantum window = new pluralia_tantum();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public pluralia_tantum() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Pluralia Tantum");
		frame.setBounds(100, 100, 1109, 185);
		
		JLabel lblIPluraliaTantum = new JLabel("I pluralia tantum (\"nomi solamente plurali\") sono quei sostantivi che,");
		lblIPluraliaTantum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAncheSeSi = new JLabel("anche se si riferiscono ad un solo oggetto, possiedono solo la forma al plurale.");
		lblAncheSeSi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblConIPluralia = new JLabel("Con i pluralia tantum il predicato, la copula e il nome del predicato ed eventuali aggettivi dovranno sempre essere concordati al plurale.");
		lblConIPluralia.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblIPluraliaTantum)
						.addComponent(lblAncheSeSi)
						.addComponent(lblConIPluralia))
					.addContainerGap(50, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblIPluraliaTantum)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAncheSeSi)
					.addGap(18)
					.addComponent(lblConIPluralia)
					.addContainerGap(184, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
