package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class aggettivi_seconda_classe {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					aggettivi_seconda_classe window = new aggettivi_seconda_classe();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public aggettivi_seconda_classe() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Aggettivi di seconda classe");
		frame.setBounds(100, 100, 810, 333);
		
		JLabel lblGliAggettiviDi = new JLabel("Gli aggettivi di seconda classe possono avere:");
		lblGliAggettiviDi.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblTerminazioniPer = new JLabel("- 3 terminazioni per maschile, femminile, neutro (acer, acris, acre).");
		lblTerminazioniPer.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblTerminazioni = new JLabel("- 2 terminazioni per maschile/femminile e neutro (dulcis, dulce).");
		lblTerminazioni.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblTerminazione = new JLabel("- 1 terminazione per tutti e tre i generi (audax).");
		lblTerminazione.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblLaDeclinazioneDi = new JLabel("La declinazione di questi aggettivi segue la terza declinazione e ha delle particolarit\u00E0:");
		lblLaDeclinazioneDi.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblAblativoSingolare = new JLabel("- ablativo singolare in \"i\".");
		lblAblativoSingolare.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoPlurale = new JLabel("- genitivo plurale in \"ium\" -nominativo, accusativo, vocativo plurale neutro in \"ia\". ");
		lblGenitivoPlurale.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblGliAggettiviDi)
						.addComponent(lblTerminazioniPer)
						.addComponent(lblTerminazioni)
						.addComponent(lblTerminazione)
						.addComponent(lblLaDeclinazioneDi)
						.addComponent(lblAblativoSingolare)
						.addComponent(lblGenitivoPlurale))
					.addContainerGap(86, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblGliAggettiviDi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblTerminazioniPer)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblTerminazioni)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblTerminazione)
					.addGap(18)
					.addComponent(lblLaDeclinazioneDi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAblativoSingolare)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblGenitivoPlurale)
					.addContainerGap(104, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
