package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class declinazione_seconda {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					declinazione_seconda window = new declinazione_seconda();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public declinazione_seconda() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Seconda declinazione");
		frame.setBounds(100, 100, 1411, 1010);
		
		JLabel lblAllaSecondaDeclinazione = new JLabel("Alla seconda declinazione appartengono");
		lblAllaSecondaDeclinazione.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblSostantiviIn = new JLabel("- Sostantivi in \u2013us");
		lblSostantiviIn.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSostantiviIn_1 = new JLabel("- Sostantivi in \u2013er (maschili)");
		lblSostantiviIn_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSostantiviIn_2 = new JLabel("- Sostantivi in \u2013ir (uno solo con i relativi composti, maschile)");
		lblSostantiviIn_2.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSostantiviIn_3 = new JLabel("- Sostantivi in \u2013um (neutri)");
		lblSostantiviIn_3.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblCasoSingolarePlurale = new JLabel("CASO SINGOLARE PLURALE");
		lblCasoSingolarePlurale.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNominativoLupusLupi = new JLabel("Nominativo Lupus Lupi");
		lblNominativoLupusLupi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoLupiLuporum = new JLabel("Genitivo Lupi Luporum");
		lblGenitivoLupiLuporum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDativoLupoLupis = new JLabel("Dativo Lupo Lupis");
		lblDativoLupoLupis.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccusativoLupumLupos = new JLabel("Accusativo Lupum Lupos");
		lblAccusativoLupumLupos.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVocativoLupeLupi = new JLabel("Vocativo Lupe Lupi");
		lblVocativoLupeLupi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblativoLupoLupis = new JLabel("Ablativo Lupo Lupis");
		lblAblativoLupoLupis.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSostantiviMaschiliE = new JLabel("SOSTANTIVI MASCHILI E FEMMINILI IN \u2013US");
		lblSostantiviMaschiliE.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSostantiviNeutriIn = new JLabel("SOSTANTIVI NEUTRI IN \u2013UM");
		lblSostantiviNeutriIn.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblCasoSingolarePlurale_1 = new JLabel("CASO SINGOLARE PLURALE");
		lblCasoSingolarePlurale_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNominativoBellumBella = new JLabel("Nominativo Bellum Bella");
		lblNominativoBellumBella.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoBelliBellorum = new JLabel("Genitivo Belli Bellorum");
		lblGenitivoBelliBellorum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDativoBelloBellis = new JLabel("Dativo Bello Bellis");
		lblDativoBelloBellis.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccusativoBellumBella = new JLabel("Accusativo Bellum Bella");
		lblAccusativoBellumBella.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVocativoBellumBella = new JLabel("Vocativo Bellum Bella");
		lblVocativoBellumBella.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblativoBelloBellis = new JLabel("Ablativo Bello Bellis");
		lblAblativoBelloBellis.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSostantiviMaschiliIn = new JLabel("SOSTANTIVI MASCHILI IN \u2013ER E IN \u2013IR ");
		lblSostantiviMaschiliIn.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblCasoSingolarePlurale_2 = new JLabel("CASO SINGOLARE PLURALE");
		lblCasoSingolarePlurale_2.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNominativoPuerPueri = new JLabel("Nominativo Puer Pueri");
		lblNominativoPuerPueri.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoPueriPuerorum = new JLabel("Genitivo Pueri Puerorum");
		lblGenitivoPueriPuerorum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDativoPueroPueris = new JLabel("Dativo Puero Pueris");
		lblDativoPueroPueris.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccusativoPuerumPueros = new JLabel("Accusativo Puerum Pueros");
		lblAccusativoPuerumPueros.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVocativoPuerPueri = new JLabel("Vocativo Puer Pueri");
		lblVocativoPuerPueri.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblativoPueroPueris = new JLabel("Ablativo Puero Pueris");
		lblAblativoPueroPueris.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblCasoSingolarePlurale_3 = new JLabel("CASO SINGOLARE PLURALE");
		lblCasoSingolarePlurale_3.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNominativoVirViri = new JLabel("Nominativo Vir Viri");
		lblNominativoVirViri.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoViriVirorum = new JLabel("Genitivo Viri Virorum");
		lblGenitivoViriVirorum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDativoViroViris = new JLabel("Dativo Viro Viris");
		lblDativoViroViris.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccusativoVirumViros = new JLabel("Accusativo Virum Viros");
		lblAccusativoVirumViros.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVocativoVirViri = new JLabel("Vocativo Vir Viri");
		lblVocativoVirViri.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblativoViroViris = new JLabel("Ablativo Viro Viris");
		lblAblativoViroViris.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSostantivoDeusidio = new JLabel("SOSTANTIVO DEUS,I (DIO)");
		lblSostantivoDeusidio.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblCasoSingolarePlurale_4 = new JLabel("CASO SINGOLARE PLURALE");
		lblCasoSingolarePlurale_4.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNominativoDeusDeidiidi = new JLabel("Nominativo Deus Dei/dii/di");
		lblNominativoDeusDeidiidi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoDeiDeorum = new JLabel("Genitivo Dei Deorum (deum)");
		lblGenitivoDeiDeorum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDativoDeoDeisdiisdis = new JLabel("Dativo Deo Deis/diis/dis");
		lblDativoDeoDeisdiisdis.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccusativoDeumDeos = new JLabel("Accusativo Deum Deos");
		lblAccusativoDeumDeos.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVocativoDivedeusDeidiidi = new JLabel("Vocativo Dive/deus Dei/dii/di");
		lblVocativoDivedeusDeidiidi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblativoDeoDeisdiisdis = new JLabel("Ablativo Deo Deis/diis/dis");
		lblAblativoDeoDeisdiisdis.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblParticolaritSecondaDeclinazione = new JLabel("PARTICOLARIT\u00C0 SECONDA DECLINAZIONE:");
		lblParticolaritSecondaDeclinazione.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblGenitivoSingolare = new JLabel("- Genitivo singolare in \u2013i o in \u2013ii nei sostantivi in \u2013ius o in \u2013ium");
		lblGenitivoSingolare.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEsFluvius = new JLabel("ES: Fluvius = gen. sing. = fluvi / fluvii");
		lblEsFluvius.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblLocusiluogo = new JLabel("- Locus,I \u201Cluogo\u201D ha al plurale due forme diverse: loci \u201Cpassi di un libro\u201D e loca \u201Cluoghi\u201D.");
		lblLocusiluogo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblCiSono = new JLabel("- Ci sono re sostantivi neutri, usati solo al singolare, in \u2013us: pelagus = mare; virus = veleno; vulnus = popolo.");
		lblCiSono.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblPluraliaTantum = new JLabel("PLURALIA TANTUM");
		lblPluraliaTantum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblArgiorumArgo = new JLabel("Argi,orum = Argo Hiberna,orum = accampamenti inverali");
		lblArgiorumArgo.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblArgiorumArgo_1 = new JLabel("Arma,orum = armi Liberi,orum = filgi");
		lblArgiorumArgo_1.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblCorioliorumCorioli = new JLabel("Corioli,orum = Corioli (citt\u00E0 dei Volsci) Pompei,orum = Pompei");
		lblCorioliorumCorioli.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDelphiorumDelfi = new JLabel("Delphi,orum = Delfi Spolia,orum = bottino, spoglie");
		lblDelphiorumDelfi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSostantiviConSignificato = new JLabel("SOSTANTIVI CON SIGNIFICATO DIVERSO AL SINGOLARE E AL PLURALE");
		lblSostantiviConSignificato.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAuxiliumiAiuto = new JLabel("Auxilium,i = aiuto Auxilia,orum = truppe ausiliarie");
		lblAuxiliumiAiuto.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblCastrumiFortezza = new JLabel("Castrum,i = fortezza Castra,orum = accampamento");
		lblCastrumiFortezza.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblImpedimentumiOstacolo = new JLabel("Impedimentum,i = ostacolo Impedimenta,orum = bagagli, salmerie");
		lblImpedimentumiOstacolo.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblLudusiGioco = new JLabel("Ludus,i = gioco, scherzo Ludi,orum = giochi, gare");
		lblLudusiGioco.setFont(new Font("Tahoma", Font.ITALIC, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblAllaSecondaDeclinazione)
						.addComponent(lblSostantiviIn)
						.addComponent(lblSostantiviIn_1)
						.addComponent(lblSostantiviIn_2)
						.addComponent(lblSostantiviIn_3)
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblSostantiviMaschiliE)
								.addComponent(lblCasoSingolarePlurale)
								.addComponent(lblNominativoLupusLupi)
								.addComponent(lblGenitivoLupiLuporum)
								.addComponent(lblDativoLupoLupis)
								.addComponent(lblAccusativoLupumLupos)
								.addComponent(lblVocativoLupeLupi)
								.addComponent(lblAblativoLupoLupis)
								.addComponent(lblSostantiviNeutriIn)
								.addComponent(lblCasoSingolarePlurale_1)
								.addComponent(lblNominativoBellumBella)
								.addComponent(lblGenitivoBelliBellorum)
								.addComponent(lblDativoBelloBellis)
								.addComponent(lblAccusativoBellumBella)
								.addComponent(lblVocativoBellumBella)
								.addComponent(lblAblativoBelloBellis))
							.addGap(45)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblAblativoViroViris)
								.addComponent(lblVocativoVirViri)
								.addComponent(lblAccusativoVirumViros)
								.addComponent(lblDativoViroViris)
								.addComponent(lblGenitivoViriVirorum)
								.addComponent(lblNominativoVirViri)
								.addComponent(lblCasoSingolarePlurale_3)
								.addGroup(groupLayout.createSequentialGroup()
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lblSostantiviMaschiliIn)
										.addComponent(lblCasoSingolarePlurale_2)
										.addComponent(lblNominativoPuerPueri)
										.addComponent(lblGenitivoPueriPuerorum)
										.addComponent(lblDativoPueroPueris)
										.addComponent(lblAccusativoPuerumPueros)
										.addComponent(lblVocativoPuerPueri)
										.addComponent(lblAblativoPueroPueris))
									.addGap(40)
									.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
										.addComponent(lblAblativoDeoDeisdiisdis)
										.addComponent(lblAccusativoDeumDeos)
										.addComponent(lblDativoDeoDeisdiisdis)
										.addComponent(lblGenitivoDeiDeorum)
										.addComponent(lblNominativoDeusDeidiidi)
										.addComponent(lblCasoSingolarePlurale_4)
										.addComponent(lblSostantivoDeusidio)
										.addComponent(lblVocativoDivedeusDeidiidi)))))
						.addGroup(groupLayout.createSequentialGroup()
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblParticolaritSecondaDeclinazione)
								.addComponent(lblGenitivoSingolare)
								.addComponent(lblEsFluvius)
								.addComponent(lblLocusiluogo)
								.addComponent(lblCiSono))
							.addGap(36)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblDelphiorumDelfi)
								.addComponent(lblCorioliorumCorioli)
								.addComponent(lblArgiorumArgo_1)
								.addComponent(lblArgiorumArgo)
								.addComponent(lblPluraliaTantum)))
						.addComponent(lblSostantiviConSignificato)
						.addComponent(lblLudusiGioco, GroupLayout.PREFERRED_SIZE, 378, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblImpedimentumiOstacolo, GroupLayout.PREFERRED_SIZE, 518, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblCastrumiFortezza, GroupLayout.PREFERRED_SIZE, 452, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblAuxiliumiAiuto, GroupLayout.PREFERRED_SIZE, 435, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(239, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblAllaSecondaDeclinazione)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblSostantiviIn)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblSostantiviIn_1)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblSostantiviIn_2)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblSostantiviIn_3)
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSostantiviMaschiliE)
						.addComponent(lblSostantiviMaschiliIn)
						.addComponent(lblSostantivoDeusidio))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCasoSingolarePlurale)
						.addComponent(lblCasoSingolarePlurale_2)
						.addComponent(lblCasoSingolarePlurale_4))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNominativoLupusLupi)
						.addComponent(lblNominativoPuerPueri)
						.addComponent(lblNominativoDeusDeidiidi))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblGenitivoLupiLuporum)
						.addComponent(lblGenitivoPueriPuerorum)
						.addComponent(lblGenitivoDeiDeorum))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDativoLupoLupis)
						.addComponent(lblDativoPueroPueris)
						.addComponent(lblDativoDeoDeisdiisdis))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAccusativoLupumLupos)
						.addComponent(lblAccusativoPuerumPueros)
						.addComponent(lblAccusativoDeumDeos))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblVocativoLupeLupi)
						.addComponent(lblVocativoPuerPueri)
						.addComponent(lblVocativoDivedeusDeidiidi))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAblativoLupoLupis)
						.addComponent(lblAblativoPueroPueris)
						.addComponent(lblAblativoDeoDeisdiisdis))
					.addGap(18)
					.addComponent(lblSostantiviNeutriIn)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCasoSingolarePlurale_1)
						.addComponent(lblCasoSingolarePlurale_3))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNominativoBellumBella)
						.addComponent(lblNominativoVirViri))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblGenitivoBelliBellorum)
						.addComponent(lblGenitivoViriVirorum))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDativoBelloBellis)
						.addComponent(lblDativoViroViris))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAccusativoBellumBella)
						.addComponent(lblAccusativoVirumViros))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblVocativoBellumBella)
						.addComponent(lblVocativoVirViri))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAblativoBelloBellis)
						.addComponent(lblAblativoViroViris))
					.addGap(18)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblParticolaritSecondaDeclinazione)
						.addComponent(lblPluraliaTantum))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblGenitivoSingolare)
						.addComponent(lblArgiorumArgo))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblEsFluvius)
						.addComponent(lblArgiorumArgo_1))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblLocusiluogo)
						.addComponent(lblCorioliorumCorioli))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblCiSono)
						.addComponent(lblDelphiorumDelfi))
					.addGap(18)
					.addComponent(lblSostantiviConSignificato)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblAuxiliumiAiuto)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCastrumiFortezza)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblImpedimentumiOstacolo)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblLudusiGioco)
					.addContainerGap(81, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}

}
