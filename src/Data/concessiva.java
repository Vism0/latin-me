package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class concessiva {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					concessiva window = new concessiva();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public concessiva() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Concessiva");
		frame.setBounds(100, 100, 1726, 688);
		
		JLabel lblNewLabel = new JLabel("Esprimono una circostanza, reale o ipotetica, a dispetto della quale si verifica quanto detto nella reggente.");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblInItalianoSe = new JLabel("In italiano, se in forma esplicita, sono introdotte da congiunzioni concessive e possono avere il verbo sia all'indicativo, sia al congiuntivo; in forma implicita sono espresse con il gerundio o il participio preceduto da \"pur\".");
		lblInItalianoSe.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblInLatinoLe = new JLabel("In latino le congiunzioni concessive hanno le seguenti reggenze:");
		lblInLatinoLe.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEsempioGaudeoEtsi = new JLabel("Esempio: gaudeo etsi non scio quare = son contento anche se non so perch\u00E9;");
		lblEsempioGaudeoEtsi.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblEsempioQuamvisSapiens = new JLabel("Esempio: quamvis sapiens sit, tamen erravit = per quanto sia saggio, tuttavia ha sbagliato;");
		lblEsempioQuamvisSapiens.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblEsempioEtiamsiHoc = new JLabel("Esempio: etiamsi hoc voluissem, assequi non potuissem = anche se avessi voluto questo, non avrei potuto ottenerlo (ipotesi irreale);");
		lblEsempioEtiamsiHoc.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblEtiamsiQuidScribam = new JLabel("etiamsi quid scribam non habebo, cotidie tibi litteras mittam = anche se non avr\u00F2 di che scriverti, ti mander\u00F2 una lettera ogni giorno (ipotesi reale);");
		lblEtiamsiQuidScribam.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblCumEUt = new JLabel("cum e ut (negazione ut non) hanno sempre il congiuntivo.");
		lblCumEUt.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblEsempioCumFacile = new JLabel("Esempio: cum facile hoc habere posset, noluit = sebbene potesse avere ci\u00F2 facilmente, non volle;");
		lblEsempioCumFacile.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblUtIdVerum = new JLabel("ut id verum non sit, tamen credere volo = quand'anche ci\u00F2 non sia vero, tuttavia voglio crederlo;");
		lblUtIdVerum.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblLicetSiCostruisce = new JLabel("licet si costruisce solo con il congiuntivo presente o perfetto.");
		lblLicetSiCostruisce.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblEsempioLicetMe = new JLabel("Esempio: licet me omnes deserant, solus agam = quand'anche tutti mi abbandonino, agir\u00F2 da solo.");
		lblEsempioLicetMe.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblEDelTutto = new JLabel("E' del tutto evidente la parentela di quest'ultimo costrutto (come pure della concessiva con ut) con il congiuntivo indipendente concessivo; esso \u00E8 infatti pi\u00F9 paratattico che ipotattico: ");
		lblEDelTutto.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblBasterebbeSostituireAlla = new JLabel("basterebbe sostituire alla virgola i due punti per trasformare questa (presunta) subordinata concessiva in una principale.");
		lblBasterebbeSostituireAlla.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblNotaBene = new JLabel("Nota bene:");
		lblNotaBene.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblLeConcessiveAl = new JLabel("le concessive al congiuntivo non sempre rispettano rigorosamente le norme della consecutio;");
		lblLeConcessiveAl.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblTalvoltaQuamvisAccompagna = new JLabel("talvolta quamvis accompagna un aggettivo o un avverbio");
		lblTalvoltaQuamvisAccompagna.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEsempioQuamvisLente = new JLabel("Esempio: quamvis lente = per quanto lentamente");
		lblEsempioQuamvisLente.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblSpessoQuamquamEtsi = new JLabel("spesso quamquam, etsi e tametsi hanno valore avverbiale: introducono perci\u00F2 una proposizione indipendente ed assumono il significato correttivo di \"per quanto\", \"d'altra parte\", \"ma a dire il vero\", \"ma poi\";");
		lblSpessoQuamquamEtsi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblEsempioQuamquamQuid = new JLabel("Esempio: quamquam, quid tibi hoc dicam? = ma poi, perch\u00E9 dovrei dirtelo?");
		lblEsempioQuamquamQuid.setFont(new Font("Tahoma", Font.ITALIC, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addComponent(lblBasterebbeSostituireAlla))
						.addComponent(lblNewLabel)
						.addComponent(lblInItalianoSe)
						.addComponent(lblInLatinoLe)
						.addComponent(lblEsempioGaudeoEtsi)
						.addComponent(lblEsempioQuamvisSapiens)
						.addComponent(lblEsempioEtiamsiHoc)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblCumEUt)
								.addComponent(lblEtiamsiQuidScribam)))
						.addComponent(lblEsempioCumFacile)
						.addGroup(groupLayout.createSequentialGroup()
							.addGap(10)
							.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
								.addComponent(lblLicetSiCostruisce)
								.addComponent(lblUtIdVerum)))
						.addComponent(lblEsempioLicetMe)
						.addComponent(lblEDelTutto)
						.addComponent(lblNotaBene)
						.addComponent(lblLeConcessiveAl)
						.addComponent(lblTalvoltaQuamvisAccompagna)
						.addComponent(lblEsempioQuamvisLente)
						.addComponent(lblSpessoQuamquamEtsi)
						.addComponent(lblEsempioQuamquamQuid))
					.addContainerGap(378, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblNewLabel)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblInItalianoSe)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblInLatinoLe)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEsempioGaudeoEtsi)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEsempioQuamvisSapiens)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEsempioEtiamsiHoc)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEtiamsiQuidScribam)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblCumEUt)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblEsempioCumFacile)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblUtIdVerum)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblLicetSiCostruisce)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblEsempioLicetMe)
					.addGap(18)
					.addComponent(lblEDelTutto)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblBasterebbeSostituireAlla)
					.addGap(18)
					.addComponent(lblNotaBene)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblLeConcessiveAl)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblTalvoltaQuamvisAccompagna)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblEsempioQuamvisLente)
					.addGap(18)
					.addComponent(lblSpessoQuamquamEtsi)
					.addGap(18)
					.addComponent(lblEsempioQuamquamQuid)
					.addContainerGap(258, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
