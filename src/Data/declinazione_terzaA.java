package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class declinazione_terzaA {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					declinazione_terzaA window = new declinazione_terzaA();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public declinazione_terzaA() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Terza declinazione / Primo Gruppo");
		frame.setBounds(100, 100, 769, 328);
		
		JLabel lblSostantiviMaschiliE = new JLabel("Sostantivi maschili e femminili");
		lblSostantiviMaschiliE.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblConsulConsulisM = new JLabel("Consul, consulis m. Singolare Plurale ");
		lblConsulConsulisM.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblNominativoConsulConsules = new JLabel("Nominativo Consul Consules");
		lblNominativoConsulConsules.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoConsulisConsulum = new JLabel("Genitivo Consulis Consulum");
		lblGenitivoConsulisConsulum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDativoConsuliConsulibus = new JLabel("Dativo Consuli Consulibus");
		lblDativoConsuliConsulibus.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccusativoConsulemConsules = new JLabel("Accusativo Consulem Consules");
		lblAccusativoConsulemConsules.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVocativoConsulConsules = new JLabel("Vocativo Consul Consules");
		lblVocativoConsulConsules.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblativoConsuleConsulibus = new JLabel("Ablativo Consule Consulibus ");
		lblAblativoConsuleConsulibus.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSostantiviNeutri = new JLabel("Sostantivi neutri");
		lblSostantiviNeutri.setFont(new Font("Tahoma", Font.BOLD, 17));
		
		JLabel lblCaputcapitisNSingolare = new JLabel("Caput,capitis n. Singolare Plurale");
		lblCaputcapitisNSingolare.setFont(new Font("Tahoma", Font.ITALIC, 17));
		
		JLabel lblNominativoCaputCapita = new JLabel("Nominativo Caput Capita");
		lblNominativoCaputCapita.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGenitivoCapitisCapitum = new JLabel("Genitivo Capitis Capitum");
		lblGenitivoCapitisCapitum.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblDativoCapitiCapitibus = new JLabel("Dativo Capiti Capitibus");
		lblDativoCapitiCapitibus.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAccusativoCaputCapita = new JLabel("Accusativo Caput Capita");
		lblAccusativoCaputCapita.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblVocativoCaputCapita = new JLabel("Vocativo Caput Capita");
		lblVocativoCaputCapita.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblAblativoCapiteCapitibus = new JLabel("Ablativo Capite Capitibus ");
		lblAblativoCapiteCapitibus.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblSostantiviMaschiliE)
						.addComponent(lblConsulConsulisM)
						.addComponent(lblNominativoConsulConsules)
						.addComponent(lblGenitivoConsulisConsulum)
						.addComponent(lblDativoConsuliConsulibus)
						.addComponent(lblAccusativoConsulemConsules)
						.addComponent(lblVocativoConsulConsules)
						.addComponent(lblAblativoConsuleConsulibus))
					.addGap(78)
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblAblativoCapiteCapitibus)
						.addComponent(lblVocativoCaputCapita)
						.addComponent(lblAccusativoCaputCapita)
						.addComponent(lblDativoCapitiCapitibus)
						.addComponent(lblGenitivoCapitisCapitum)
						.addComponent(lblNominativoCaputCapita)
						.addComponent(lblSostantiviNeutri)
						.addComponent(lblCaputcapitisNSingolare, GroupLayout.PREFERRED_SIZE, 255, GroupLayout.PREFERRED_SIZE))
					.addContainerGap(135, Short.MAX_VALUE))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblSostantiviMaschiliE)
						.addComponent(lblSostantiviNeutri))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblConsulConsulisM)
						.addComponent(lblCaputcapitisNSingolare))
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNominativoConsulConsules)
						.addComponent(lblNominativoCaputCapita))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblGenitivoConsulisConsulum)
						.addComponent(lblGenitivoCapitisCapitum))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblDativoConsuliConsulibus)
						.addComponent(lblDativoCapitiCapitibus))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAccusativoConsulemConsules)
						.addComponent(lblAccusativoCaputCapita))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblVocativoConsulConsules)
						.addComponent(lblVocativoCaputCapita))
					.addPreferredGap(ComponentPlacement.RELATED)
					.addGroup(groupLayout.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblAblativoConsuleConsulibus)
						.addComponent(lblAblativoCapiteCapitibus))
					.addContainerGap(27, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
