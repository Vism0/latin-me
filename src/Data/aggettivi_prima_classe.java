package Data;
import java.awt.EventQueue;
import java.awt.Font;

import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.LayoutStyle.ComponentPlacement;

public class aggettivi_prima_classe {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					aggettivi_prima_classe window = new aggettivi_prima_classe();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public aggettivi_prima_classe() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame("Aggettivi di prima classe");
		frame.setBounds(100, 100, 1330, 243);
		
		JLabel lblGliAggettiviIn = new JLabel("Gli aggettivi in latino si dividono in due gruppi, detti classi.");
		lblGliAggettiviIn.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblGliAggettiviDi = new JLabel("Gli aggettivi di prima classe hanno 3 terminazioni, una per il maschile, una per il femminile ed una per il neutro a seconda del genere del sostantivo con cui concordano.");
		lblGliAggettiviDi.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblSeguonoPerIl = new JLabel("Seguono per il maschile ed il neutro la seconda declinazione e per il femminile la prima declinazione.");
		lblSeguonoPerIl.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblLaggettivoConcordaSempre = new JLabel("L'aggettivo concorda sempre in caso, genere e numero con il sostantivo a cui si riferisce.");
		lblLaggettivoConcordaSempre.setFont(new Font("Tahoma", Font.PLAIN, 17));
		
		JLabel lblQuandoSiRiferisce = new JLabel("Quando si riferisce a un infinito sostantivato ha la forma del neutro singolare. ");
		lblQuandoSiRiferisce.setFont(new Font("Tahoma", Font.PLAIN, 17));
		GroupLayout groupLayout = new GroupLayout(frame.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addGroup(groupLayout.createParallelGroup(Alignment.LEADING)
						.addComponent(lblGliAggettiviIn)
						.addComponent(lblGliAggettiviDi)
						.addComponent(lblSeguonoPerIl)
						.addComponent(lblLaggettivoConcordaSempre)
						.addComponent(lblQuandoSiRiferisce)))
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addGroup(groupLayout.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblGliAggettiviIn)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblGliAggettiviDi)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblSeguonoPerIl)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(lblLaggettivoConcordaSempre)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(lblQuandoSiRiferisce)
					.addContainerGap(141, Short.MAX_VALUE))
		);
		frame.getContentPane().setLayout(groupLayout);
	}
}
